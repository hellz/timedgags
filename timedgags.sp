/* 

timedgags.sp

Made by Jomn
Version: 1.3.8

*/

#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#include <timedgags>
#undef REQUIRE_PLUGIN
#include <adminmenu>
#include <basecomm>
#include "dbi.inc"

#define TG_VERSION "1.3.8"

#define DISABLE_ADDGAG (1<<0)
#define DISABLE_ADDMUTE (1<<1)
#define DISABLE_ADDSILENCE (1<<2)
#define DISABLE_UNGAG (1<<3)
#define DISABLE_UNMUTE (1<<4)
#define DISABLE_UNSILENCE (1<<5)

#define GAG (1<<0)
#define MUTE (1<<1)

#define STATE_NONE 0  
#define STATE_GAGGED (1<<0)
#define STATE_MUTED (1<<1)
#define STATE_GAG_UNCHECKED (1<<2)
#define STATE_MUTE_UNCHECKED (1<<3)

enum State
{
	ConfigStateNone = 0,
	ConfigStateConfig,
	ConfigStateReasons,
	ConfigStateTimes,
	ConfigStateLimits
}

enum GagMenuType
{
	GagMenuType_Mute,
	GagMenuType_Unmute,
	GagMenuType_Gag,
	GagMenuType_Ungag,
	GagMenuType_Silence,
	GagMenuType_Unsilence
}

new g_GagTarget[MAXPLAYERS + 1];
new GagMenuType:g_GagType[MAXPLAYERS+ 1];
new g_GagTime[MAXPLAYERS + 1];

new g_PlayerStatus[MAXPLAYERS + 1]; //Gag status of players

new Handle:g_PlayerRecheck[MAXPLAYERS + 1] = {INVALID_HANDLE, ...};
new Handle:g_PlayerDataPack[MAXPLAYERS + 1] = {INVALID_HANDLE, ...};

new Handle:hTopMenu = INVALID_HANDLE;

new Handle:hReasonMenu = INVALID_HANDLE;
new Handle:hTimeMenu = INVALID_HANDLE;

new Handle:hDatabase = INVALID_HANDLE;
new Handle:hSQLiteDB = INVALID_HANDLE;

new Handle:g_UngagWhenReadyTimer[MAXPLAYERS+1] = {INVALID_HANDLE, ...};
new Handle:g_UnmuteWhenReadyTimer[MAXPLAYERS+1] = {INVALID_HANDLE, ...};

new String:ServerIP[24];
new String:ServerPort[7];
new String:DatabasePrefix[10] = "tg";
new ServerID = -1;

new String:log[256];

new State:ConfigState;
new Handle:ConfigParser;

new g_CommandDisable;

//Command flags (see "http://wiki.alliedmods.net/Adding_Admins_(SourceMod)#Levels" for info about flags)

new g_TimedgagAdminFlags;
new g_TimedmuteAdminFlags;
new g_TimedsilenceAdminFlags;
new g_UntimedgagAdminFlags;
new g_UntimedmuteAdminFlags;
new g_UntimedsilenceAdminFlags;
new g_AddgagAdminFlags;
new g_AddmuteAdminFlags;
new g_AddsilenceAdminFlags;
new g_PermanentGagAdminFlags;

new Handle:g_GroupListTrie = INVALID_HANDLE;

new bool:g_LateLoaded;
new bool:g_bPluginJustLoaded = false;
new Float:g_RetryTime = 15.0;
new ProcessQueueTime = 5;

new bool:g_OwnReason[MAXPLAYERS+1] = {false, ...};

public Plugin:myinfo =
{
	name = "Timed Gags",
	author = "Jomn",
	description = "Adds temporary and permanent mutes/gags/silences",
	version = TG_VERSION,
	url = "http://www.hellz.fr/"
};

public APLRes:AskPluginLoad2(Handle:myself, bool:late, String:error[], err_max)
{
	RegPluginLibrary("timedgags");
	CreateNative("TGGagPlayer", Native_TGGagPlayer);
	CreateNative("TGMutePlayer", Native_TGMutePlayer);
	CreateNative("TGSilencePlayer", Native_TGSilencePlayer);
	g_LateLoaded = late;
	
	return APLRes_Success;
}

public OnPluginStart()
{
	g_GroupListTrie = CreateTrie();

	LoadTranslations("common.phrases");
	LoadTranslations("basecomm.phrases");
	LoadTranslations("timedgags.phrases");
	
	CreateConVar("tg_version", TG_VERSION, _, FCVAR_PLUGIN|FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY|FCVAR_DONTRECORD);
	
	
	FormatServerIP();
	GetConVarString (FindConVar ("hostport"), ServerPort, sizeof(ServerPort));
	
	AddCommandListener(Command_Say, "say");
	AddCommandListener(Command_Say, "say_team");
	AddCommandListener(Command_Unmute, "sm_unmute");
	AddCommandListener(Command_Ungag, "sm_ungag");
	AddCommandListener(Command_Unsilence, "sm_unsilence");
	
	RegAdminCmd("sm_timedmute", Command_Timedmute, ADMFLAG_CHAT, "sm_timedmute <player> <minutes|0> [reason]");
	RegAdminCmd("sm_timedgag", Command_Timedgag, ADMFLAG_CHAT, "sm_timedgag <player> <minutes|0> [reason]");
	RegAdminCmd("sm_timedsilence", Command_Timedsilence, ADMFLAG_CHAT, "sm_timedsilence <player> <minutes|0> [reason]");
	
	RegAdminCmd("sm_untimedmute", Command_Untimedmute, ADMFLAG_UNBAN, "sm_untimedmute <steamid> [reason]");
	RegAdminCmd("sm_untimedgag", Command_Untimedgag, ADMFLAG_UNBAN, "sm_untimedgag <steamid> [reason]");
	RegAdminCmd("sm_untimedsilence", Command_Untimedsilence, ADMFLAG_UNBAN, "sm_untimedsilence <steamid> [reason]");
	
	RegAdminCmd("sm_addmute", Command_Addmute, ADMFLAG_RCON, "sm_addmute <steamid> <time> [reason]");
	RegAdminCmd("sm_addgag", Command_Addgag, ADMFLAG_RCON, "sm_addgag <steamid> <time> [reason]");
	RegAdminCmd("sm_addsilence", Command_Addsilence, ADMFLAG_RCON, "sm_addsilence <steamid> <time> [reason]");
	
	RegAdminCmd("tg_reload", Command_Reload, ADMFLAG_RCON, "Reload timedgags configs and gag reason menu options");
	
	
	if ((hReasonMenu = CreateMenu(ReasonSelected)) != INVALID_HANDLE)
	{
		SetMenuPagination(hReasonMenu, 8);
		SetMenuExitBackButton(hReasonMenu, true);
	}
	if ((hTimeMenu = CreateMenu (TimeSelected, MENU_ACTIONS_DEFAULT|MenuAction_DrawItem)) != INVALID_HANDLE)
	{
		SetMenuPagination (hTimeMenu, 8);
		SetMenuExitBackButton (hTimeMenu, true);
	}
	BuildPath(Path_SM, log, sizeof(log), "logs/timedgags.log");
	
	if (!SQL_CheckConfig("timedgags"))
	{
		if (hReasonMenu != INVALID_HANDLE)
			CloseHandle (hReasonMenu);
		if (hTimeMenu != INVALID_HANDLE)
			CloseHandle (hTimeMenu);
		LogToFile(log, "Database failure: Could not find Database conf \"timedgags\".");
		SetFailState("Database failure: Could not find Database conf \"timedgags\"");
		return;
	}
	
	SQL_TConnect(GotDatabase, "timedgags");
	
	InitializeBackupDB();
	
	CreateTimer(float(ProcessQueueTime * 60), ProcessQueue);
	CreateTimer(float(ProcessQueueTime * 60), ProcessQueueDelete);
	
	
	if (g_LateLoaded)
	{
		g_bPluginJustLoaded = true;
	}
}

public OnMapStart()
{
	if (g_LateLoaded && g_bPluginJustLoaded)
	{
		decl String:auth[32];
		
		for (new i = 1; i <= MaxClients; i++)
		{
			if (IsClientConnected(i) && !IsFakeClient(i))
			{
				g_PlayerStatus[i] = STATE_MUTE_UNCHECKED|STATE_GAG_UNCHECKED;
			}
			if (IsClientInGame(i) && !IsFakeClient(i))
			{
				GetClientAuthString(i, auth, sizeof(auth));
				OnClientAuthorized(i, auth);
			}
		}
		g_bPluginJustLoaded = false;
	}
	ResetSettings();
	
	AddCommandOverride ("sm_timedgag", Override_Command, g_TimedgagAdminFlags);
	AddCommandOverride ("sm_timedmute", Override_Command, g_TimedmuteAdminFlags);
	AddCommandOverride ("sm_timedsilence", Override_Command, g_TimedsilenceAdminFlags);
	AddCommandOverride ("sm_untimedgag", Override_Command, g_UntimedgagAdminFlags);
	AddCommandOverride ("sm_untimedmute", Override_Command, g_UntimedmuteAdminFlags);
	AddCommandOverride ("sm_untimedsilence", Override_Command, g_UntimedsilenceAdminFlags);
	AddCommandOverride ("sm_addgag", Override_Command, g_AddgagAdminFlags);
	AddCommandOverride ("sm_addmute", Override_Command, g_AddmuteAdminFlags);
	AddCommandOverride ("sm_addsilence", Override_Command, g_AddsilenceAdminFlags);
}

public OnAllPluginsLoaded()
{
	new Handle:topmenu;
	if (LibraryExists("adminmenu") && ((topmenu = GetAdminTopMenu()) != INVALID_HANDLE))
	{
		OnAdminMenuReady(topmenu);
	}
}

public bool:OnClientConnect(client, String:rejectmsg[], maxlen)
{
	g_PlayerStatus[client] = STATE_MUTE_UNCHECKED|STATE_GAG_UNCHECKED;
	return true;
}


public OnClientAuthorized(client, const String:auth[])
{
	if (!IsClientInGame (client))
	{
		g_PlayerRecheck[client] = CreateTimer(1.0, ClientRecheck, client);
		return;
	}
	
	if (auth[0] == 'B' || auth[9] == 'L' || hDatabase == INVALID_HANDLE)
	{
		g_PlayerStatus[client] = STATE_NONE;
		return;
	}
	
	decl String:query[1024];
	
	FormatEx(query, sizeof(query), "SELECT ends, length FROM %s_gag WHERE (authid REGEXP '^STEAM_[0-9]:%s$') AND (length = 0 OR ends > UNIX_TIMESTAMP())", DatabasePrefix, auth[8]);
	SQL_TQuery (hDatabase, VerifyGag, query, GetClientUserId(client), DBPrio_High);
	
	FormatEx(query, sizeof(query), "SELECT ends, length FROM %s_mute WHERE (authid REGEXP '^STEAM_[0-9]:%s$') AND (length = 0 OR ends > UNIX_TIMESTAMP())", DatabasePrefix, auth[8]);
	SQL_TQuery (hDatabase, VerifyMute, query, GetClientUserId(client), DBPrio_High);
	
}


public OnClientDisconnect (client)
{
	if (g_UngagWhenReadyTimer[client] != INVALID_HANDLE)
	{
		KillTimer (g_UngagWhenReadyTimer[client]);
		g_UngagWhenReadyTimer[client] = INVALID_HANDLE;
	}
	
	if (g_UnmuteWhenReadyTimer[client] != INVALID_HANDLE)
	{
		KillTimer (g_UnmuteWhenReadyTimer[client]);
		g_UnmuteWhenReadyTimer[client] = INVALID_HANDLE;
	}
}

public OnMapEnd()
{
	for (new i = 0; i <= MaxClients; i++)
	{
		if (g_PlayerDataPack[i] != INVALID_HANDLE)
		{
			CloseHandle(g_PlayerDataPack[i]);
			g_PlayerDataPack[i] = INVALID_HANDLE;
		}
	}
}

// Normal Functions

FormatServerIP ()
{
	new pieces[4];
	new longip = GetConVarInt(FindConVar("hostip"));
	
	pieces[0] = (longip >> 24) & 0xFF;
	pieces[1] = (longip >> 16) & 0xFF;
	pieces[2] = (longip >> 8) & 0xFF;
	pieces[3] = longip & 0xFF;
	
	Format (ServerIP, sizeof(ServerIP), "%d.%d.%d.%d", pieces[0], pieces[1], pieces[2], pieces[3]);
}

CreateGag(client, target, time, String:reason[], type, bool:showReason=true)
{
	decl String:adminAuth[64];
	
	
	if (g_PlayerStatus[target] == STATE_GAGGED|STATE_MUTED)
	{
		if (client)
			PrintToChat (client, "[TG] %t", "Player Already Silenced");
		return;
	}
	else if ((g_PlayerStatus[target] & STATE_GAGGED) == STATE_GAGGED)
	{
		if (type == GAG)
		{
			if (client)
				PrintToChat (client, "[TG] %t", "Player Already Gagged");
			return;
		}
		else if (type == (GAG|MUTE))
		{
			type = MUTE; // We don't want to gag the player again.
		}
	}
	else if ((g_PlayerStatus[target] & STATE_MUTED) == STATE_MUTED)
	{
		if (type == MUTE)
		{
			if (client)
				PrintToChat (client, "[TG] %t", "Player Already Muted");
			return;
		}
		else if (type == (GAG|MUTE))
		{
			type = GAG; // We don't want to mute the player again.
		}
	}
	
	new maxGagTimeLimit = client ? GetClientGagTimeLimit (client) : -1;
	
	if (maxGagTimeLimit >= 0 && time > maxGagTimeLimit)
	{
		PrintToChat (client, "[TG] %t", "Too Long");
		return;
	}
	
	
	if (!client)
	{
		if (reason[0] == '\0')
		{
			PrintToServer("[TG] %t", "Include Reason");
			return;
		}
		
		strcopy(adminAuth, sizeof(adminAuth), "STEAM_ID_SERVER");
	}
	else
	{
		GetClientAuthString(client, adminAuth, sizeof(adminAuth));
	}
	
	if (reason[0] != '\0')
	{
		if (type == GAG)
			GagTarget (client, target, time, reason, showReason);
		else if (type == MUTE)
			MuteTarget (client, target, time, reason, showReason);
		else
			SilenceTarget (client, target, time, reason, showReason);
	}
	
	// Prepare and pack everything that will go in the database.
	
	decl String:name[64], String:auth[64], String:ip[24];
	
	GetClientName(target, name, sizeof(name));
	GetClientIP(target, ip, sizeof(ip));
	GetClientAuthString(target, auth, sizeof(auth));
	
	new userid = client ? GetClientUserId (client) : 0;
	
	new Handle:dataPack = CreateDataPack();
	new Handle:reasonPack = CreateDataPack();
	
	WritePackString(reasonPack, reason);
	
	WritePackCell(dataPack, client);
	WritePackCell(dataPack, target);
	WritePackCell(dataPack, userid);
	WritePackCell(dataPack, GetClientUserId(target));
	WritePackCell(dataPack, time);
	WritePackCell(dataPack, type);
	WritePackCell(dataPack, _:reasonPack);
	WritePackString(dataPack, name);
	WritePackString(dataPack, auth);
	WritePackString(dataPack, ip);
	WritePackString(dataPack, adminAuth);
	
	ResetPack(dataPack);
	ResetPack(reasonPack);
	
	if (reason[0] != '\0')
	{
		if (hDatabase != INVALID_HANDLE)
			SQL_InsertGag (time, name, auth, ip, reason, adminAuth, type, dataPack);
		else
			SQL_InsertTempGag (time, name, auth, ip, reason, adminAuth, type, dataPack);
	}
	else
	{
		g_GagTarget[client] = target;
		g_GagTime[client] = time;
		if (type == (GAG|MUTE))
			g_GagType[client] = GagMenuType_Silence;
		else if (type == GAG)
			g_GagType[client] = GagMenuType_Gag;
		else if (type == MUTE)
			g_GagType[client] = GagMenuType_Mute;
		g_PlayerDataPack[client] = dataPack;
		DisplayMenu(hReasonMenu, client, MENU_TIME_FOREVER);
		ReplyToCommand(client, "[TG] %t", "Check Menu");
	}
}

GetClientGagTimeLimit (client)
{
	new max = -1;
	new AdminId:adminId;
	decl String:group[33];
	
	if ((adminId = GetUserAdmin (client)) != INVALID_ADMIN_ID)
	{
		for (new i = 0; i < GetAdminGroupCount (adminId); i++)
		{
			if (GetAdminGroup (adminId, i, group, sizeof(group)) != INVALID_GROUP_ID)
			{
				new value;
				if (GetTrieValue (g_GroupListTrie, group, value))
				{
					if (value > max)
						max = value;
					if (value == -1)
						return -1;
				}
			}
		}
	}
	
	return max;
}

GagTarget (admin, client, time, String:reason[], bool:showReason=true)
{
	decl String:name[MAX_NAME_LENGTH];
	
	GetClientName (client, name, sizeof(name));

	if (!time)
	{
		if (reason[0] == '\0' || !showReason)
			ShowActivity2 (admin, "[TG] ", "%t", "Permagagged Player", name);
		else
			ShowActivity2 (admin, "[TG] ", "%t", "Permagagged Player Reason", name, reason);
	}
	else
	{
		if (reason[0] == '\0' || !showReason)
			ShowActivity2 (admin, "[TG] ", "%t", "Gagged Player", name, time);
		else
			ShowActivity2 (admin, "[TG] ", "%t", "Gagged Player Reason", name, time, reason);
	}
	
	LogAction(admin, client, "\"%L\" gagged \"%L\" (minutes \"%d\") (reason \"%s\")", admin, client, time, reason); //"
	
	g_PlayerStatus[client] |= STATE_GAGGED;
	BaseComm_SetClientGag (client, true);
	new timeleft;
	GetMapTimeLeft(timeleft);
	if (time)
	{
		if (timeleft < 0)
		{
			new Handle:timerPack;
			CreateDataTimer (5.0, RetryUngagsTimers, timerPack, TIMER_FLAG_NO_MAPCHANGE);
			WritePackCell (timerPack, client);
			WritePackCell (timerPack, (time*60)-5);
			ResetPack (timerPack);
		}
		else if ((time*60) < timeleft)
		{
			g_UngagWhenReadyTimer[client] = CreateTimer (float(time*60), Timer_UngagPlayer, client);
		}
	}
}

MuteTarget (admin, client, time, String:reason[], bool:showReason=true)
{
	decl String:name[MAX_NAME_LENGTH];
	
	GetClientName (client, name, sizeof(name));

	if (!time)
	{
		if (reason[0] == '\0' || !showReason)
			ShowActivity2 (admin, "[TG] ", "%t", "Permamuted Player", name);
		else
			ShowActivity2 (admin, "[TG] ", "%t", "Permamuted Player Reason", name, reason);
	}
	else
	{
		if (reason[0] == '\0' || !showReason)
			ShowActivity2 (admin, "[TG] ", "%t", "Muted Player", name, time);
		else
			ShowActivity2 (admin, "[TG] ", "%t", "Muted Player Reason", name, time, reason);
	}
	
	LogAction(admin, client, "\"%L\" muted \"%L\" (minutes \"%d\") (reason \"%s\")", admin, client, time, reason); //"
	
	g_PlayerStatus[client] |= STATE_MUTED;
	BaseComm_SetClientMute (client, true);
	new timeleft;
	GetMapTimeLeft(timeleft);
	if (time)
	{
		if (timeleft < 0)
		{
			new Handle:timerPack;
			CreateDataTimer (5.0, RetryUnmutesTimers, timerPack, TIMER_FLAG_NO_MAPCHANGE);
			WritePackCell (timerPack, client);
			WritePackCell (timerPack, (time*60)-5);
			ResetPack (timerPack);
		}
		else if ((time*60) < timeleft)
		{
			g_UnmuteWhenReadyTimer[client] = CreateTimer (float(time*60), Timer_UnmutePlayer, client);
		}
	}
}

SilenceTarget (admin, client, time, String:reason[], bool:showReason=true)
{
	decl String:name[MAX_NAME_LENGTH];
	
	GetClientName (client, name, sizeof(name));

	if (!time)
	{
		if (reason[0] == '\0' || !showReason)
			ShowActivity2 (admin, "[TG] ", "%t", "Permasilenced Player", name);
		else
			ShowActivity2 (admin, "[TG] ", "%t", "Permasilenced Player Reason", name, reason);
	}
	else
	{
		if (reason[0] == '\0' || !showReason)
			ShowActivity2 (admin, "[TG] ", "%t", "Silenced Player", name, time);
		else
			ShowActivity2 (admin, "[TG] ", "%t", "Silenced Player Reason", name, time, reason);
	}
	
	LogAction(admin, client, "\"%L\" silenced \"%L\" (minutes \"%d\") (reason \"%s\")", admin, client, time, reason); //"
	
	g_PlayerStatus[client] = (STATE_GAGGED|STATE_MUTED);
	BaseComm_SetClientGag (client, true);
	BaseComm_SetClientMute (client, true);
	new timeleft;
	GetMapTimeLeft(timeleft);
	if (time)
	{
		if (timeleft < 0)
		{
			new Handle:ungagPack;
			CreateDataTimer (5.0, RetryUngagsTimers, ungagPack, TIMER_FLAG_NO_MAPCHANGE);
			WritePackCell (ungagPack, client);
			WritePackCell (ungagPack, (time*60)-5);
			ResetPack (ungagPack);
			
			new Handle:unmutePack;
			CreateDataTimer (5.0, RetryUnmutesTimers, unmutePack, TIMER_FLAG_NO_MAPCHANGE);
			WritePackCell (unmutePack, client);
			WritePackCell (unmutePack, (time*60)-5);
			ResetPack (unmutePack);
		}
		else if ((time*60) < timeleft)
		{
			g_UngagWhenReadyTimer[client] = CreateTimer (float(time*60), Timer_UngagPlayer, client);
			g_UnmuteWhenReadyTimer[client] = CreateTimer (float(time*60), Timer_UnmutePlayer, client);
		}
	}
}

GetUserIdOfAuthString (const String:authid[])
{
	for (new i = 1; i <= MaxClients; i++)
	{
		if (IsClientInGame(i) && !IsFakeClient(i))
		{
			decl String:buffer[32];
			GetClientAuthString (i, buffer, sizeof(buffer));
			
			if (StrEqual (authid, buffer))
			{
				return GetClientUserId (i);
			}
		}
	}
	
	return -1;
}

RemoveGag (client, String:authid[], type, userid=-1)
{
	new Handle:dataPack = CreateDataPack ();
	
	WritePackCell (dataPack, client);
	WritePackCell (dataPack, userid);
	WritePackString (dataPack, authid);
	
	decl String:query[1024];
	
	if (type & GAG)
	{
		new Handle:ungagPack = CloneHandle (dataPack);
		Format(query, sizeof(query), "SELECT gagid FROM %s_gag WHERE authid = '%s'", DatabasePrefix, authid);
		SQL_TQuery(hDatabase, SelectUngagCallback, query, ungagPack);
	}
	if (type & MUTE)
	{
		new Handle:unmutePack = CloneHandle (dataPack);
		Format(query, sizeof(query), "SELECT muteid FROM %s_mute WHERE authid = '%s'", DatabasePrefix, authid);
		SQL_TQuery(hDatabase, SelectUnmuteCallback, query, unmutePack);
	}
	
	CloseHandle (dataPack);
}

stock bool:HasClientPermissions (client, flags, bool:needAll=false) 
{
	new AdminId:admin = GetUserAdmin (client);
	new adminFlags = GetAdminFlags (admin, Access_Effective);
	
	if (GetAdminFlag (admin, Admin_Root))
		return true;
	
	if (needAll)
	{
		for (new i = 0; i < AdminFlags_TOTAL; i++)
			if ((GetBit (flags, i) & GetBit (~adminFlags, i)))
				return false;
		return true;
	}
	
	if ((adminFlags & flags))
		return true;
	return false;
}

stock GetBit (bits, num)
{
	return (bits & (1<<num));
}

/********************************************
************ DATABASE CODE START ************
********************************************/

CreateTables()
{
	decl String:query[1024];
	decl String:error[128];
	
	FormatEx(query, sizeof(query), "CREATE TABLE IF NOT EXISTS %s_mute \
									(muteid int(8) NOT NULL auto_increment, \
									authid varchar(64) character set utf8 NOT NULL default '', \
									ip varchar(32) default NULL, \
									name varchar(128) character set utf8 NOT NULL default 'unamed', \
									starts int(11) NOT NULL default '0', \
									ends int(11) NOT NULL default '0', \
									length int(10) NOT NULL default '0', \
									reason text character set utf8 NOT NULL, \
									admin varchar(64) character set utf8 NOT NULL default '', \
									serverid int(6) NOT NULL default '0', \
									server varchar(128) character set utf8 NOT NULL default '', \
									PRIMARY KEY (muteid), \
									KEY serverid (serverid), \
									FULLTEXT KEY reason (reason), \
									FULLTEXT KEY authid (authid)) \
									ENGINE=MyISAM DEFAULT CHARSET=utf8;", DatabasePrefix);
	
	SQL_LockDatabase(hDatabase);
	if (!SQL_FastQuery (hDatabase, query))
	{
		SQL_GetError (hDatabase, error, sizeof(error));
		LogToFile (log, "Mute Table Creation Query Failed: %s", error);
	}
	SQL_UnlockDatabase (hDatabase);
	
	FormatEx(query, sizeof(query), "CREATE TABLE IF NOT EXISTS %s_gag \
									(gagid int(8) NOT NULL auto_increment, \
									authid varchar(64) character set utf8 NOT NULL default '', \
									ip varchar(32) default NULL, \
									name varchar(128) character set utf8 NOT NULL default 'unamed', \
									starts int(11) NOT NULL default '0', \
									ends int(11) NOT NULL default '0', \
									length int(10) NOT NULL default '0', \
									reason text character set utf8 NOT NULL, \
									admin varchar(128) character set utf8 NOT NULL default '', \
									serverid int(6) NOT NULL default '0', \
									server varchar(256) character set utf8 NOT NULL default '', \
									PRIMARY KEY (gagid), \
									KEY serverid (serverid), \
									FULLTEXT KEY reason (reason), \
									FULLTEXT KEY authid (authid)) \
									ENGINE=MyISAM DEFAULT CHARSET=utf8;", DatabasePrefix);
									
	SQL_LockDatabase(hDatabase);
	if (!SQL_FastQuery (hDatabase, query))
	{
		SQL_GetError (hDatabase, error, sizeof(error));
		LogToFile (log, "Gag Table Creation Query Failed: %s", error);
	}
	SQL_UnlockDatabase (hDatabase);
}

DeleteExpiredGags()
{
	// We want to clear gags and mutes from the database that aren't needed anymore.
	
	decl String:query[512];
	Format (query, sizeof(query), "DELETE FROM %s_gag WHERE ends <= UNIX_TIMESTAMP() AND length > 0", DatabasePrefix);
	SQL_TQuery (hDatabase, ErrorCheckCallback, query);
	
	Format (query, sizeof(query), "DELETE FROM %s_mute WHERE ends <= UNIX_TIMESTAMP() AND length > 0", DatabasePrefix);
	SQL_TQuery (hDatabase, ErrorCheckCallback, query);
	
	Format (query, sizeof(query), "OPTIMIZE TABLE %s_gag", DatabasePrefix);
	SQL_TQuery (hDatabase, ErrorCheckCallback, query);
	
	Format (query, sizeof(query), "OPTIMIZE TABLE %s_mute", DatabasePrefix);
	SQL_TQuery (hDatabase, ErrorCheckCallback, query);
}

InitializeBackupDB()
{
	decl String:error[255];
	hSQLiteDB = SQLite_UseDatabase("timedgags-queue", error, sizeof(error));
	if(hSQLiteDB == INVALID_HANDLE)
		SetFailState(error);
	
	SQL_LockDatabase(hSQLiteDB);
	SQL_FastQuery(hSQLiteDB, "CREATE TABLE IF NOT EXISTS queue (authid TEXT, time INTEGER, starts INTEGER, reason TEXT, type INTEGER, name TEXT, ip TEXT, admin TEXT);");
	SQL_UnlockDatabase(hSQLiteDB);
	
	SQL_LockDatabase (hSQLiteDB);
	SQL_FastQuery (hSQLiteDB, "CREATE TABLE IF NOT EXISTS queue_delete (authid TEXT, type INTEGER);");
	SQL_UnlockDatabase (hSQLiteDB);
}

public GotDatabase(Handle:owner, Handle:hndl, const String:error[], any:data)
{
	if (hndl == INVALID_HANDLE)
	{
		LogToFile(log, "Database failure: %s", error);
	}
	else
	{
		hDatabase = hndl;
		SQL_FastQuery (hDatabase, "SET NAMES \"UTF8\"");
		CreateTables();
		DeleteExpiredGags();
	}
}

public ProcessQueueCallback(Handle:owner, Handle:hndl, const String:error[], any:data)
{
	if (hndl == INVALID_HANDLE || error[0])
	{
		LogToFile(log, "Failed to retrieve queued gags from sqlite database: %s", error);
		return;
	}
	
	decl String:auth[32];
	new time;
	new startTime;
	new type;
	decl String:reason[128];
	decl String:name[64];
	decl String:ip[20];
	decl String:adminAuth[32];
	decl String:query[1024];
	decl String:gagName[128];
	decl String:gagReason[256];
	
	while (SQL_MoreRows(hndl))
	{
		if (!SQL_FetchRow(hndl))
			continue;
		
		SQL_FetchString(hndl, 0, auth, sizeof(auth));
		time = SQL_FetchInt(hndl, 1);
		startTime = SQL_FetchInt(hndl, 2);
		SQL_FetchString(hndl, 3, reason, sizeof(reason));
		type = SQL_FetchInt(hndl, 4);
		SQL_FetchString(hndl, 5, name, sizeof(name));
		SQL_FetchString(hndl, 6, ip, sizeof(ip));
		SQL_FetchString(hndl, 7, adminAuth, sizeof(adminAuth));
		
		SQL_EscapeString(hSQLiteDB, name, gagName, sizeof(gagName));
		SQL_EscapeString(hSQLiteDB, reason, gagReason, sizeof(gagReason));
		
		if (startTime + time * 60 > GetTime() || time == 0)
		{
			if (type & GAG)
			{
				FormatEx(query, sizeof(query), "INSERT INTO %s_gag (authid, ip, name, starts, ends, length, reason, admin, serverid, server) \
										VALUES ('%s', '%s', '%s', UNIX_TIMESTAMP(), UNIX_TIMESTAMP() + %d, %d, '%s', '%s', %d, '%s:%s'))",
										DatabasePrefix, auth, ip, gagName, (time*60), (time*60), gagReason, adminAuth, ServerID, ServerIP, ServerPort);
									
				new Handle:authPack = CreateDataPack();
				WritePackCell (authPack, GAG);
				WritePackString(authPack, auth);
				ResetPack(authPack);
				SQL_TQuery (hDatabase, AddedFromSQLiteCallback, query, authPack);
			}
			if (type & MUTE)
			{
				FormatEx(query, sizeof(query), "INSERT INTO %s_mute (authid, ip, name, starts, ends, length, reason, admin, serverid, server) \
										VALUES ('%s', '%s', '%s', UNIX_TIMESTAMP(), UNIX_TIMESTAMP() + %d, %d, '%s', '%s', %d, '%s:%s'))",
										DatabasePrefix, auth, ip, gagName, (time*60), (time*60), gagReason, adminAuth, ServerID, ServerIP, ServerPort);
									
				new Handle:authPack = CreateDataPack();
				WritePackCell (authPack, MUTE);
				WritePackString(authPack, auth);
				ResetPack(authPack);
				SQL_TQuery(hDatabase, AddedFromSQLiteCallback, query, authPack);
			}
		}
		else
		{
			if (type & GAG)
			{
				FormatEx(query, sizeof(query), "DELETE FROM queue WHERE authid = '%s' AND type = %d", auth, GAG);
				SQL_TQuery(hSQLiteDB, ErrorCheckCallback, query);
			}
			if (type & MUTE)
			{
				FormatEx(query, sizeof(query), "DELETE FROM queue WHERE authid = '%s' AND type = %d", auth, MUTE);
				SQL_TQuery(hSQLiteDB, ErrorCheckCallback, query);
			}
		}
	}
	
	CreateTimer(float(ProcessQueueTime * 60), ProcessQueue);
}

public AddedFromSQLiteCallback (Handle:owner, Handle:hndl, const String:error[], any:authPack)
{
	new type = ReadPackCell (authPack);
	decl String:auth[32];
	ReadPackString (authPack, auth, sizeof(auth));
	if (!error[0])
	{
		decl String:query[512];
		Format (query, sizeof(query), "DELETE FROM queue WHERE authid = '%s' AND type = %d", auth, type);
		SQL_TQuery (hSQLiteDB, ErrorCheckCallback, query);
	}
	CloseHandle (authPack);
}

public ProcessQueueDeleteCallback (Handle:owner, Handle:hndl, const String:error[], any:data)
{
	if (error[0])
	{
		LogToFile (log, "Failed to retrieve queued deleted gags from sqlite database: %s", error);
		return;
	}
	
	new type;
	decl String:authid[32];
	decl String:query[1024];
	
	while (SQL_MoreRows (hndl))
	{
		if (!SQL_FetchRow (hndl))
			continue;
		
		SQL_FetchString (hndl, 0, authid, sizeof(authid));
		type = SQL_FetchInt (hndl, 1);
		
		if (type & GAG)
		{
			new Handle:authPack = CreateDataPack();
			WritePackCell (authPack, GAG);
			WritePackString (authPack, authid);
			ResetPack (authPack);
			
			Format (query, sizeof(query), "DELETE FROM %s_gag WHERE authid = '%s'", DatabasePrefix, authid);
			SQL_TQuery (hDatabase, DeletedFromSQLiteCallback, query, authPack);
		}
		if (type & MUTE)
		{
			new Handle:authPack = CreateDataPack();
			WritePackCell (authPack, MUTE);
			WritePackString (authPack, authid);
			ResetPack (authPack);
			
			Format (query, sizeof(query), "DELETE FROM %s_mute WHERE authid = '%s'", DatabasePrefix, authid);
			SQL_TQuery (hDatabase, DeletedFromSQLiteCallback, query, authPack);
		}
	}
	
	CreateTimer (float(ProcessQueueTime * 60), ProcessQueueDelete);
}

public DeletedFromSQLiteCallback (Handle:owner, Handle:hndl, const String:error[], any:dataPack)
{
	new type = ReadPackCell (dataPack);
	decl String:authid[32];
	ReadPackString (dataPack, authid, sizeof(authid));
	if (!error[0])
	{
		decl String:query[512];
		Format (query, sizeof(query), "DELETE FROM queue_delete WHERE authid = '%s' AND type = %d", authid, type);
		SQL_TQuery (hSQLiteDB, ErrorCheckCallback, query);
	}
	CloseHandle (dataPack);
}

public SelectUngagCallback(Handle:owner, Handle:hndl, const String:error[], any:dataPack)
{
	new client;
	decl String:authid[32];
	
	ResetPack (dataPack);
	
	client = ReadPackCell (dataPack);
	ReadPackCell (dataPack);
	ReadPackString (dataPack, authid, sizeof(authid));
	
	if (error[0])
	{
		LogToFile (log, "Ungag Select Query Failed: %s", error);
		if (client && IsClientInGame (client))
		{
			PrintToChat (client, "[TG] sm_untimedgag/silence failed");
		}
		return;
	}
	
	if (hndl == INVALID_HANDLE || !SQL_GetRowCount (hndl))
	{
		if (client && IsClientInGame (client))
		{
			PrintToChat (client, "[TG] No active gags found for that filter (%s)", authid);
		}
		else
		{
			PrintToServer ("[TG] No active gags found for that filter (%s)", authid);
		}
		return;
	}
	
	if (hndl != INVALID_HANDLE && SQL_FetchRow (hndl))
	{
		new gagid = SQL_FetchInt (hndl, 0);
		
		decl String:query[1024];
		Format(query, sizeof(query), "DELETE FROM %s_gag WHERE gagid = %d", DatabasePrefix, gagid);
		SQL_TQuery (hDatabase, RemoveGagCallback, query, dataPack);
	}
	return;
}

public RemoveGagCallback (Handle:owner, Handle:hndl, const String:error[], any:dataPack)
{
	new client, userid, target;
	decl String:authid[32];
	
	if (dataPack != INVALID_HANDLE)
	{
		ResetPack (dataPack);
		client = ReadPackCell (dataPack);
		userid = ReadPackCell (dataPack);
		ReadPackString (dataPack, authid, sizeof(authid));
		CloseHandle (dataPack);
	}
	else
	{
		ThrowError ("Invalid Handle In RemoveGagCallback");
	}
	
	if (error[0])
	{
		LogToFile (log, "Remove Ungag Query Failed: %s", error);
		return;
	}
	
	LogAction (client, -1, "\"%L\" removed gag (authid \"%s\")", client, authid); //"
	
	target = GetClientOfUserId (userid);
	
	if (target && IsClientInGame (target) && !IsFakeClient (target))
	{
		if (g_UngagWhenReadyTimer[target] != INVALID_HANDLE)
		{
			KillTimer (g_UngagWhenReadyTimer[target]);
			g_UngagWhenReadyTimer[target] = INVALID_HANDLE;
		}
		
		BaseComm_SetClientGag (target, false);
		g_PlayerStatus[target] &= ~STATE_GAGGED;
	}
	
	if (client && IsClientInGame (client))
	{
		PrintToChat (client, "[TG] Successfully ungagged %s", authid);
	}
	else
	{
		PrintToServer ("[TG] Successfully ungagged %s", authid);
	}
}

public SelectUnmuteCallback(Handle:owner, Handle:hndl, const String:error[], any:dataPack)
{
	new client;
	decl String:authid[32];
	
	ResetPack (dataPack);
	
	client = ReadPackCell (dataPack);
	ReadPackCell (dataPack);
	ReadPackString (dataPack, authid, sizeof(authid));
	
	if (error[0])
	{
		LogToFile (log, "Unmute Select Query Failed: %s", error);
		if (client && IsClientInGame (client))
		{
			PrintToChat (client, "[TG] sm_untimedmute/silence failed");
		}
		return;
	}
	
	if (hndl == INVALID_HANDLE || !SQL_GetRowCount (hndl))
	{
		if (client && IsClientInGame (client))
		{
			PrintToChat (client, "[TG] No active mutes found for that filter (%s)", authid);
		}
		else
		{
			PrintToServer ("[TG] No active mutes found for that filter (%s)", authid);
		}
		return;
	}
	
	if (hndl != INVALID_HANDLE && SQL_FetchRow (hndl))
	{
		new muteid = SQL_FetchInt (hndl, 0);
		
		decl String:query[1024];
		Format(query, sizeof(query), "DELETE FROM %s_mute WHERE muteid = %d", DatabasePrefix, muteid);
		SQL_TQuery (hDatabase, RemoveMuteCallback, query, dataPack);
	}
	return;
}

public RemoveMuteCallback (Handle:owner, Handle:hndl, const String:error[], any:dataPack)
{
	new client, userid, target;
	decl String:authid[32];
	
	if (dataPack != INVALID_HANDLE)
	{
		ResetPack (dataPack);
		client = ReadPackCell (dataPack);
		userid = ReadPackCell (dataPack);
		ReadPackString (dataPack, authid, sizeof(authid));
		CloseHandle (dataPack);
	}
	else
	{
		ThrowError ("Invalid Handle In RemoveMuteCallback");
	}
	
	if (error[0])
	{
		LogToFile (log, "Remove Unmute Query Failed: %s", error);
		if (client && IsClientInGame (client))
		{
			PrintToChat (client, "[TG] sm_untimedmute/silence failed");
		}
		return;
	}
	
	LogAction (client, -1, "\"%L\" removed mute (authid \"%s\")", client, authid); //"
	
	target = GetClientOfUserId (userid);
	
	if (target && IsClientInGame (target) && !IsFakeClient (target))
	{
		if (g_UnmuteWhenReadyTimer[target] != INVALID_HANDLE)
		{
			KillTimer (g_UnmuteWhenReadyTimer[target]);
			g_UnmuteWhenReadyTimer[target] = INVALID_HANDLE;
		}
		
		BaseComm_SetClientMute (target, false);
		g_PlayerStatus[target] &= ~STATE_MUTED;
	}
	
	if (client && IsClientInGame (client))
	{
		PrintToChat (client, "[TG] Successfully unmuted %s", authid);
	}
	else
	{
		PrintToServer ("[TG] Successfully unmuted %s", authid);
	}
}
	
public SQL_InsertGag (time, const String:name[], const String:auth[], const String:ip[], const String:reason[], const String:adminAuth[], type, Handle:dataPack)
{
	decl String:gagName[128], String:gagReason[256];
	
	SQL_EscapeString(hDatabase, name, gagName, sizeof(gagName));
	SQL_EscapeString(hDatabase, reason, gagReason, sizeof(gagReason));
	
	if (type & GAG)
	{
		decl String:query[1024];
		FormatEx(query, sizeof(query), "INSERT INTO %s_gag (authid, ip, name, starts, ends, length, reason, admin, serverid, server) \
										VALUES ('%s', '%s', '%s', UNIX_TIMESTAMP(), UNIX_TIMESTAMP() + %d, %d, '%s', '%s', %d, '%s:%s')",
										DatabasePrefix, auth, ip, gagName, (time*60), (time*60), gagReason, adminAuth, ServerID, ServerIP, ServerPort);

		SQL_TQuery(hDatabase, VerifyInsert, query, dataPack, DBPrio_High);
	}
	if (type & MUTE)
	{
		decl String:query[1024];
		FormatEx(query, sizeof(query), "INSERT INTO %s_mute (authid, ip, name, starts, ends, length, reason, admin, serverid, server) \
										VALUES ('%s', '%s', '%s', UNIX_TIMESTAMP(), UNIX_TIMESTAMP() + %d, %d, '%s', '%s', %d, '%s:%s')",
										DatabasePrefix, auth, ip, gagName, (time*60), (time*60), gagReason, adminAuth, ServerID, ServerIP, ServerPort);

		SQL_TQuery(hDatabase, VerifyInsert, query, dataPack, DBPrio_High);
	}
}

public SQL_InsertTempGag (time, const String:name[], const String:auth[], const String:ip[], const String:reason[], const String:adminAuth[], type, Handle:dataPack)
{
	SetPackPosition(dataPack, 48);
	new Handle:reasonPack = Handle:ReadPackCell(dataPack);
	if (reasonPack != INVALID_HANDLE)
		CloseHandle(reasonPack);
	CloseHandle(dataPack);
	
	decl String:gagName[128];
	decl String:gagReason[256];
	decl String:query[1024];
	
	SQL_EscapeString(hSQLiteDB, name, gagName, sizeof(gagName));
	SQL_EscapeString(hSQLiteDB, reason, gagReason, sizeof(gagReason));
	
	if (type & GAG)
	{
		FormatEx(query, sizeof(query), "INSERT INTO queue VALUES ('%s', %i, %i, '%s', %d, '%s', '%s', '%s')",
									auth, time, GetTime(), gagReason, GAG, gagName, ip, adminAuth);
		SQL_TQuery(hSQLiteDB, ErrorCheckCallback, query);
	}
	if (type & MUTE)
	{
		FormatEx(query, sizeof(query), "INSERT INTO queue VALUES ('%s', %i, %i, '%s', %d, '%s', '%s', '%s')",
									auth, time, GetTime(), gagReason, MUTE, gagName, ip, adminAuth);
		SQL_TQuery(hSQLiteDB, ErrorCheckCallback, query);
	}
}

public VerifyGag (Handle:owner, Handle:hndl, const String:error[], any:userid)
{
	new client = GetClientOfUserId(userid);
	
	if (hndl == INVALID_HANDLE || error[0])
	{
		LogToFile(log, "Verify Gag Query Failed: %s", error);
		g_PlayerRecheck[client] = CreateTimer(g_RetryTime, ClientRecheck, client);
		return;
	}
	
	if (SQL_GetRowCount (hndl) > 0 && SQL_FetchRow (hndl))
	{
		BaseComm_SetClientGag(client, true);
		g_PlayerStatus[client] |= STATE_GAGGED;
		new ends = SQL_FetchInt (hndl, 0);
		new length = SQL_FetchInt (hndl, 1);
		new timeleft;
		new unmuteTime = ends - GetTime();
		
		GetMapTimeLeft (timeleft);
		
		if (length)
		{
			if (timeleft < 0)
			{
				new Handle:timerPack;
				CreateDataTimer (5.0, RetryUngagsTimers, timerPack, TIMER_FLAG_NO_MAPCHANGE);
				WritePackCell (timerPack, client);
				WritePackCell (timerPack, unmuteTime-5);
				ResetPack (timerPack);
			}
			else if (unmuteTime < timeleft)
			{
				g_UngagWhenReadyTimer[client] = CreateTimer (float(unmuteTime), Timer_UngagPlayer, client);
			}
		}
		g_PlayerStatus[client] &= ~STATE_GAG_UNCHECKED;
	}
	else
	{
		if (client)
		{
			decl String:query[1024];
			decl String:authid[32];
			GetClientAuthString (client, authid, sizeof(authid));
			Format (query, sizeof(query), "SELECT time, starts FROM queue WHERE authid = '%s' AND type = %d", authid, MUTE);
			SQL_TQuery (hSQLiteDB, VerifyGagSQLiteDB, query, userid, DBPrio_High);
		}
	}
}

public VerifyGagSQLiteDB (Handle:owner, Handle:hndl, const String:error[], any:userid)
{
	new client = GetClientOfUserId (userid);
	
	if (hndl == INVALID_HANDLE || error[0])
	{
		LogToFile (log, "Verify Gag SQLiteDB Query Failed: %s", error);
		g_PlayerRecheck[client] = CreateTimer (g_RetryTime, ClientRecheck, client);
		return;
	}
	
	if (SQL_GetRowCount (hndl) > 0 && SQL_FetchRow (hndl))
	{
		BaseComm_SetClientGag (client, true);
		g_PlayerStatus[client] |= STATE_GAGGED;
		new time = SQL_FetchInt (hndl, 0);
		new startTime = SQL_FetchInt (hndl, 1);
		new ends = startTime + time*60;
		new timeleft;
		new unmuteTime = ends - GetTime();
		
		GetMapTimeLeft (timeleft);
		
		if (time)
		{
			if (timeleft < 0)
			{
				new Handle:timerPack;
				CreateDataTimer (5.0, RetryUngagsTimers, timerPack, TIMER_FLAG_NO_MAPCHANGE);
				WritePackCell (timerPack, client);
				WritePackCell (timerPack, unmuteTime-5);
				ResetPack (timerPack);
			}
			else if (unmuteTime < timeleft)
			{
				g_UngagWhenReadyTimer[client] = CreateTimer (float(unmuteTime), Timer_UngagPlayer, client);
			}
		}
		
	}
	g_PlayerStatus[client] &= ~STATE_GAG_UNCHECKED;
}

public VerifyMute (Handle:owner, Handle:hndl, const String:error[], any:userid)
{
	new client = GetClientOfUserId(userid);
	
	if (hndl == INVALID_HANDLE || error[0])
	{
		LogToFile(log, "Verify Mute Query Failed: %s", error);
		g_PlayerRecheck[client] = CreateTimer(g_RetryTime, ClientRecheck, client);
		return;
	}
	
	if (SQL_GetRowCount (hndl) > 0 && SQL_FetchRow (hndl))
	{
		BaseComm_SetClientMute(client, true);
		g_PlayerStatus[client] |= STATE_MUTED;
		new ends = SQL_FetchInt (hndl, 0);
		new length = SQL_FetchInt (hndl, 1);
		new timeleft;
		new unmuteTime = ends - GetTime();
		
		GetMapTimeLeft (timeleft);
		
		if (length)
		{
			if (timeleft < 0)
			{
				new Handle:timerPack;
				CreateDataTimer (5.0, RetryUnmutesTimers, timerPack, TIMER_FLAG_NO_MAPCHANGE);
				WritePackCell (timerPack, client);
				WritePackCell (timerPack, unmuteTime-5);
				ResetPack (timerPack);
			}
			else if (unmuteTime < timeleft)
			{
				g_UnmuteWhenReadyTimer[client] = CreateTimer (float(unmuteTime), Timer_UnmutePlayer, client);
			}
		}
		g_PlayerStatus[client] &= ~STATE_MUTE_UNCHECKED;
	}
	else
	{
		if (client)
		{
			decl String:query[1024];
			decl String:authid[32];
			GetClientAuthString (client, authid, sizeof(authid));
			Format (query, sizeof(query), "SELECT time, starts FROM queue WHERE authid = '%s' AND type = %d", authid, MUTE);
			SQL_TQuery (hSQLiteDB, VerifyMuteSQLiteDB, query, userid);
		}
	}
}

public VerifyMuteSQLiteDB (Handle:owner, Handle:hndl, const String:error[], any:userid)
{
	new client = GetClientOfUserId (userid);
	
	if (hndl == INVALID_HANDLE || error[0])
	{
		LogToFile (log, "Verify Mute SQLiteDB Query Failed: %s", error);
		g_PlayerRecheck[client] = CreateTimer (g_RetryTime, ClientRecheck, client);
		return;
	}
	
	if (SQL_GetRowCount (hndl) > 0)
	{
		BaseComm_SetClientMute (client, true);
		g_PlayerStatus[client] |= STATE_MUTED;
		new time = SQL_FetchInt (hndl, 0);
		new startTime = SQL_FetchInt (hndl, 1);
		new ends = startTime + time*60;
		new timeleft;
		new unmuteTime = ends - GetTime();
		
		GetMapTimeLeft (timeleft);
		
		if (time)
		{
			if (timeleft < 0)
			{
				new Handle:timerPack;
				CreateDataTimer (5.0, RetryUnmutesTimers, timerPack, TIMER_FLAG_NO_MAPCHANGE);
				WritePackCell (timerPack, client);
				WritePackCell (timerPack, unmuteTime-5);
				ResetPack (timerPack);
			}
			else if (unmuteTime < timeleft)
			{
				g_UnmuteWhenReadyTimer[client] = CreateTimer (float(unmuteTime), Timer_UnmutePlayer, client);
			}
		}
		
	}
	g_PlayerStatus[client] &= ~STATE_MUTE_UNCHECKED;
}

public VerifyInsert (Handle:owner, Handle:hndl, const String:error[], any:dataPack)
{
	if (dataPack == INVALID_HANDLE)
	{
		LogToFile(log, "Gag/Mute/Silence failed: %s", error);
		return;
	}
	
	if (hndl == INVALID_HANDLE || error[0])
	{
		LogToFile(log, "Verify Insert Query Failed: %s", error);
		
		SetPackPosition(dataPack, 32);
		new time = ReadPackCell(dataPack);
		new type = ReadPackCell(dataPack);
		new Handle:reasonPack = Handle:ReadPackCell(dataPack);
		decl String:reason[128];
		decl String:name[50];
		ReadPackString(dataPack, name, sizeof(name));
		decl String:auth[30];
		ReadPackString(dataPack, auth, sizeof(auth));
		decl String:ip[20];
		ReadPackString(dataPack, ip, sizeof(ip));
		decl String:adminAuth[30];
		ReadPackString(dataPack, adminAuth, sizeof(adminAuth));
		ResetPack(dataPack);
		ResetPack(reasonPack);
		
		SQL_InsertTempGag (time, name, auth, ip, reason, adminAuth, type, Handle:dataPack);
		return;
	}
	
	new client = ReadPackCell(dataPack);
	SetPackPosition(dataPack, 48);
	new Handle:reasonPack = Handle:ReadPackCell (dataPack);
	
	if (g_PlayerDataPack[client] != INVALID_HANDLE)
	{
		CloseHandle(g_PlayerDataPack[client]);
		CloseHandle(reasonPack);
		g_PlayerDataPack[client] = INVALID_HANDLE;
	}
}

public VerifyDelete (Handle:owner, Handle:hndl, const String:error[], any:dataPack)
{
	new type = ReadPackCell (dataPack);

	if (hndl == INVALID_HANDLE || error[0])
	{
		LogToFile (log, "Verify Delete Query Failed: %s", error);
		
		decl String:query[1024];
		decl String:authid[32];
		ReadPackString (dataPack, authid, sizeof(authid));
		
		Format (query, sizeof(query), "INSERT INTO queue_delete VALUES ('%s', %d)", authid, type);
		SQL_TQuery (hSQLiteDB, ErrorCheckCallback, query);
	}
	
	CloseHandle (dataPack);	

}

public ErrorCheckCallback(Handle:owner, Handle:hndl, const String:error[], any:data)
{
	if(error[0])
	{
		LogToFile(log, "Query Failed: %s", error);
	}
}

public SelectAddgagCallback (Handle:owner, Handle:hndl, const String:error[], any:dataPack)
{
	new client, minutes, type;
	decl String:adminAuth[32], String:authid[32], String:gagReason[256], String:query[1024], String:reason[128];
	
	ResetPack(dataPack);
	client = ReadPackCell(dataPack);
	minutes = ReadPackCell(dataPack);
	type = ReadPackCell (dataPack);
	ReadPackString (dataPack, reason, sizeof(reason));
	ReadPackString (dataPack, authid, sizeof(authid));
	ReadPackString (dataPack, adminAuth, sizeof(adminAuth));
	SQL_EscapeString (hDatabase, reason, gagReason, sizeof(gagReason));
	
	if (error[0])
	{
		LogToFile(log, "Add Gag Select Query Failed: %s", error);
		if (client && IsClientInGame(client))
			PrintToChat(client, "[TG] Failed to gag %s.", authid);
		else
			PrintToServer("[TG] Failed to gag %s.", authid);
		
		return;
	}
	
	if (SQL_GetRowCount(hndl))
	{
		if (client && IsClientInGame(client))
			PrintToChat (client, "[TG] %s is already gagged.", authid);
		else
			PrintToServer ("[TG] %s is already gagged.", authid);
		
		return;
	}
	
	if (type & GAG)
	{
		FormatEx(query, sizeof(query), "INSERT INTO %s_gag (authid, name, starts, ends, length, reason, admin, serverid, server) VALUES \
										('%s', '', UNIX_TIMESTAMP(), UNIX_TIMESTAMP() + %d, %d, '%s', '%s', %d, '%s:%s')",
										DatabasePrefix, authid, minutes * 60, minutes * 60, gagReason, adminAuth, ServerID, ServerIP, ServerPort);
		
		SQL_TQuery (hDatabase, InsertAddgagCallback, query, dataPack, DBPrio_High);
	}
	if (type & MUTE)
	{
		FormatEx(query, sizeof(query), "INSERT INTO %s_mute (authid, name, starts, ends, length, reason, admin, serverid, server) VALUES \
										('%s', '', UNIX_TIMESTAMP(), UNIX_TIMESTAMP() + %d, %d, '%s', '%s', %d, '%s:%s')",
										DatabasePrefix, authid, minutes * 60, minutes * 60, gagReason, adminAuth, ServerID, ServerIP, ServerPort);
		
		SQL_TQuery (hDatabase, InsertAddgagCallback, query, dataPack, DBPrio_High);
	}	
}

public InsertAddgagCallback (Handle:owner, Handle:hndle, const String:error[], any:dataPack)
{
	new client, minutes, type;
	decl String:authid[32], String:reason[128];
	
	ResetPack(dataPack);
	client = ReadPackCell(dataPack);
	minutes = ReadPackCell(dataPack);
	type = ReadPackCell(dataPack);
	ReadPackString(dataPack, reason, sizeof(reason));
	ReadPackString(dataPack, authid, sizeof(authid));
	
	if (error[0])
	{
		LogToFile(log, "Add Gag Insert Query Failed: %s", error);
		if (client && IsClientInGame(client))
		{
			PrintToChat(client, "[SM] sm_addgag/mute/silence failed !");
		}
		return;
	}
	
	LogAction (client, -1, "\"%L\" added gag (minutes \"%i\") (id \"%s\") (reason \"%s\") (type \"%d\")", client, minutes, authid, reason, type);
	
	
	if (client && IsClientInGame(client))
	{
		PrintToChat(client, "[TG] %s was successfully gagged", authid);
	}
	else
	{
		PrintToServer("[TG] %s was successfully gagged", authid);
	}
}

public SelectCheckMutesCallback (Handle:owner, Handle:hndl, const String:error[], any:client)
{
	if (error[0])
	{
		LogToFile (log, "Select Check Mutes Query Failed: %s", error);
		return;
	}
	
	if (hndl == INVALID_HANDLE || !SQL_GetRowCount (hndl))
	{
		if (IsClientInGame (client))
		{
			BaseComm_SetClientMute (client, false);
			g_PlayerStatus[client] &= ~STATE_MUTED;
			PrintToChat (client, "[TG] %t", "Now Unmuted");
		}
	}
}

public SelectCheckGagsCallback (Handle:owner, Handle:hndl, const String:error[], any:client)
{
	if (error[0])
	{
		LogToFile (log, "Select Check Gags Query Failed: %s", error);
		return;
	}
	
	if (hndl == INVALID_HANDLE || !SQL_GetRowCount (hndl))
	{
		if (IsClientInGame (client))
		{
			BaseComm_SetClientGag (client, false);
			g_PlayerStatus[client] &= ~STATE_GAGGED;
			PrintToChat (client, "[TG] %t", "Now Ungagged");
		}
	}
}

/********************************************
************* DATABASE CODE END *************
********************************************/

/********************************************
************* ACTIONS CODE START ************
********************************************/

public Action:Command_Timedmute(client, args)
{
	if (args < 2)
	{
		ReplyToCommand(client, "[SM] Usage: sm_timedmute <player> <time|0> [reason]");
		return Plugin_Handled;
	}
	
	// Get the target
	
	decl String:buffer[64];
	GetCmdArg(1, buffer, sizeof(buffer));
	new target = FindTarget(client, buffer, true);
	if (target == -1)
	{
		return Plugin_Handled;
	}
	
	if (!CanUserTarget (client, target))
	{
		ReplyToCommand (client, "[SM] You can not target this admin !");
		return Plugin_Handled;
	}
	
	// Get the time
	
	GetCmdArg(2, buffer, sizeof(buffer));
	new time = StringToInt(buffer);
	if (!time && !StrEqual (buffer, "0"))
	{
		ReplyToCommand(client, "[TG] %t", "Invalid Amount");
		return Plugin_Handled;
	}
	if (!time && client && !HasClientPermissions(client, g_PermanentGagAdminFlags, true))
	{
		ReplyToCommand(client, "[TG] %t", "No Perm Mute");
		return Plugin_Handled;
	}
	
	// Get the reason
	
	decl String:reason[128];
	if (args >= 3)
	{
		GetCmdArg(3, reason, sizeof(reason));
		StripQuotes (reason);
	}
	else
	{
		reason[0] = '\0';
	}
	
	CreateGag (client, target, time, reason, MUTE);
	return Plugin_Handled;
}
	
public Action:Command_Timedgag(client, args)
{
	if (args < 2)
	{
		ReplyToCommand(client, "[SM] Usage: sm_timedgag <player> <time|0> [reason]");
		return Plugin_Handled;
	}
	
	// Get the target
	
	decl String:buffer[64];
	GetCmdArg(1, buffer, sizeof(buffer));
	new target = FindTarget(client, buffer, true);
	if (target == -1)
	{
		return Plugin_Handled;
	}
	
	if (!CanUserTarget (client, target))
	{
		ReplyToCommand (client, "[SM] You can not target this admin !");
		return Plugin_Handled;
	}
	
	// Get the time
	
	GetCmdArg(2, buffer, sizeof(buffer));
	new time = StringToInt(buffer);
	if (!time && !StrEqual (buffer, "0"))
	{
		ReplyToCommand(client, "[TG] %t", "Invalid Amount");
		return Plugin_Handled;
	}
	if (!time && client && !HasClientPermissions(client, g_PermanentGagAdminFlags, true))
	{
		ReplyToCommand(client, "[TG] %t", "No Perm Gag");
		return Plugin_Handled;
	}
	
	// Get the reason
	
	decl String:reason[128];
	if (args >= 3)
	{
		GetCmdArg(3, reason, sizeof(reason));
		StripQuotes (reason);
	}
	else
	{
		reason[0] = '\0';
	}
	
	CreateGag(client, target, time, reason, GAG);
	return Plugin_Handled;
}	

public Action:Command_Timedsilence(client, args)
{
	if (args < 2)
	{
		ReplyToCommand(client, "[SM] Usage: sm_timedsilence <player> <time|0> [reason]");
		return Plugin_Handled;
	}
	
	// Get the target
	
	decl String:buffer[64];
	GetCmdArg(1, buffer, sizeof(buffer));
	new target = FindTarget(client, buffer, true);
	if (target == -1)
	{
		return Plugin_Handled;
	}
	
	if (!CanUserTarget (client, target))
	{
		ReplyToCommand (client, "[SM] You can not target this admin !");
		return Plugin_Handled;
	}
	
	// Get the time
	
	GetCmdArg(2, buffer, sizeof(buffer));
	new time = StringToInt(buffer);
	if (!time && !StrEqual (buffer, "0"))
	{
		ReplyToCommand(client, "[TG] %t", "Invalid Amount");
		return Plugin_Handled;
	}
	if (!time && client && !HasClientPermissions(client, g_PermanentGagAdminFlags, true))
	{
		ReplyToCommand(client, "[TG] %t", "No Perm Silence");
		return Plugin_Handled;
	}
	
	// Get the reason
	
	decl String:reason[128];
	if (args >= 3)
	{
		GetCmdArg(3, reason, sizeof(reason));
	}
	else
	{
		reason[0] = '\0';
	}
	
	CreateGag(client, target, time, reason, GAG|MUTE);
	return Plugin_Handled;
}

public Action:Command_Untimedgag(client, args)
{
	if (args < 1)
	{
		ReplyToCommand(client, "[SM] Usage: sm_untimedgag <steam_id>");
		return Plugin_Handled;
	}
	
	if (g_CommandDisable & DISABLE_UNGAG)
	{
		ReplyToCommand(client, "[TG] %t", "Command Disabled");
		return Plugin_Handled;
	}
	
	decl String:authid[64];
	
	GetCmdArg (1, authid, sizeof(authid));
	StripQuotes (authid);
	
	
	if (strncmp (authid, "STEAM_", 6) != 0)
	{
		ReplyToCommand(client, "[TG] %t", "Invalid SteamID specified");
		return Plugin_Handled;
	}
	
	new userid = GetUserIdOfAuthString (authid);
	
	RemoveGag (client, authid, GAG, userid);
	
	return Plugin_Handled;
}

public Action:Command_Untimedmute(client, args)
{
	if (args < 1)
	{
		ReplyToCommand(client, "[SM] Usage: sm_untimedgag <steam_id>");
		return Plugin_Handled;
	}
	
	if (g_CommandDisable & DISABLE_UNMUTE)
	{
		ReplyToCommand(client, "[TG] %t", "Command Disabled");
		return Plugin_Handled;
	}
	
	decl String:authid[64];
	
	GetCmdArg (1, authid, sizeof(authid));
	StripQuotes (authid);
	
	if (strncmp (authid, "STEAM_", 6) != 0)
	{
		ReplyToCommand(client, "[TG] %t", "Invalid SteamID specified");
		return Plugin_Handled;
	}
	
	new userid = GetUserIdOfAuthString (authid);
	
	RemoveGag (client, authid, MUTE, userid);
	
	return Plugin_Handled;
}

public Action:Command_Untimedsilence(client, args)
{
	if (args < 1)
	{
		ReplyToCommand(client, "[SM] Usage: sm_untimedgag <steam_id>");
		return Plugin_Handled;
	}
	
	if (g_CommandDisable & DISABLE_UNSILENCE)
	{
		ReplyToCommand(client, "[TG] %t", "Command Disabled");
		return Plugin_Handled;
	}
	
	decl String:authid[64];
	
	GetCmdArg(1, authid, sizeof(authid));
	StripQuotes (authid);
	
	if (strncmp (authid, "STEAM_", 6) != 0)
	{
		ReplyToCommand(client, "[TG] %t", "Invalid SteamID specified");
		return Plugin_Handled;
	}
	
	new userid = GetUserIdOfAuthString (authid);	
	
	RemoveGag (client, authid, MUTE|GAG, userid);
	
	return Plugin_Handled;
}

public Action:Command_Addgag(client, args)
{
	if (args < 2)
	{
		ReplyToCommand(client, "[SM] Usage: sm_addgag <steamid> <time> [reason]");
		return Plugin_Handled;
	}
	
	if (g_CommandDisable & DISABLE_ADDGAG)
	{
		ReplyToCommand(client, "[TG] %t", "Command Disabled");
		return Plugin_Handled;
	}
	
	decl String:arguments[256];
	decl String:authid[50];
	decl String:time[50];
	
	GetCmdArgString(arguments, sizeof(arguments));
	
	new len, total_len;
	
	len = BreakString(arguments, authid, sizeof(authid));
	
	if (len == -1)
	{
		ReplyToCommand(client, "[SM] Usage: sm_addgag <steamid> <time> [reason]");
		return Plugin_Handled;
	}
	total_len += len;
	
	len = BreakString(arguments[total_len], time, sizeof(time));
	
	if (len != -1)
	{
		total_len += len;
	}
	else
	{
		total_len = 0;
		arguments[0] = '\0';
	}
	
	decl String:adminAuth[32];
	
	new minutes = StringToInt(time);
	
	if(!minutes && client && !(HasClientPermissions(client, g_PermanentGagAdminFlags, true)))
	{
		ReplyToCommand(client, "[TG] %t", "No Perm Gag");
		return Plugin_Handled;
	}
	
	if (!client)
	{
		strcopy (adminAuth, sizeof(adminAuth), "STEAM_ID_SERVER");
	}
	else
	{
		GetClientAuthString(client, adminAuth, sizeof(adminAuth));
	}
	
	new Handle:dataPack = CreateDataPack();
	
	WritePackCell(dataPack, client);
	WritePackCell(dataPack, minutes);
	WritePackCell(dataPack, GAG);
	WritePackString(dataPack, arguments[total_len]);
	WritePackString(dataPack, authid);
	WritePackString(dataPack, adminAuth);
	
	decl String:query[1024];
	
	FormatEx(query, sizeof(query), "SELECT gagid FROM %s_gag WHERE authid = '%s' AND (length = 0 OR ends > UNIX_TIMESTAMP())", DatabasePrefix, authid);
	
	SQL_TQuery(hDatabase, SelectAddgagCallback, query, dataPack, DBPrio_High);
	return Plugin_Handled;
}

public Action:Command_Addmute(client, args)
{
	if (args < 2)
	{
		ReplyToCommand(client, "[SM] Usage: sm_addmute <steamid> <time> [reason]");
		return Plugin_Handled;
	}
	
	if (g_CommandDisable & DISABLE_ADDGAG)
	{
		ReplyToCommand(client, "[TG] %t", "Command Disabled");
		return Plugin_Handled;
	}
	
	decl String:arguments[256];
	decl String:authid[50];
	decl String:time[50];
	
	GetCmdArgString(arguments, sizeof(arguments));
	
	new len, total_len;
	
	len = BreakString(arguments, authid, sizeof(authid));
	
	if (len == -1)
	{
		ReplyToCommand(client, "[SM] Usage: sm_addmute <steamid> <time> [reason]");
		return Plugin_Handled;
	}
	total_len += len;
	
	len = BreakString(arguments[total_len], time, sizeof(time));
	
	if (len != -1)
	{
		total_len += len;
	}
	else
	{
		total_len = 0;
		arguments[0] = '\0';
	}
	
	decl String:adminAuth[32];
	
	new minutes = StringToInt(time);
	
	if(!minutes && client && !(HasClientPermissions(client, g_PermanentGagAdminFlags, true)))
	{
		ReplyToCommand(client, "[TG] %t", "No Perm Mute");
		return Plugin_Handled;
	}
	
	if (!client)
	{
		strcopy (adminAuth, sizeof(adminAuth), "STEAM_ID_SERVER");
	}
	else
	{
		GetClientAuthString(client, adminAuth, sizeof(adminAuth));
	}
	
	new Handle:dataPack = CreateDataPack();
	
	WritePackCell(dataPack, client);
	WritePackCell(dataPack, minutes);
	WritePackCell(dataPack, MUTE);
	WritePackString(dataPack, arguments[total_len]);
	WritePackString(dataPack, authid);
	WritePackString(dataPack, adminAuth);
	
	decl String:query[1024];
	
	FormatEx(query, sizeof(query), "SELECT muteid FROM %s_mute WHERE authid = '%s' AND (length = 0 OR ends > UNIX_TIMESTAMP())", DatabasePrefix, authid);
	
	SQL_TQuery(hDatabase, SelectAddgagCallback, query, dataPack, DBPrio_High);
	return Plugin_Handled;
}

public Action:Command_Addsilence(client, args)
{
	if (args < 2)
	{
		ReplyToCommand(client, "[SM] Usage: sm_addsilence <steamid> <time> [reason]");
		return Plugin_Handled;
	}
	
	if (g_CommandDisable & DISABLE_ADDMUTE)
	{
		ReplyToCommand(client, "[TG] %t", "Command Disabled");
		return Plugin_Handled;
	}
	
	decl String:arguments[256];
	decl String:authid[50];
	decl String:time[50];
	
	GetCmdArgString(arguments, sizeof(arguments));
	
	new len, total_len;
	
	len = BreakString(arguments, authid, sizeof(authid));
	
	if (len == -1)
	{
		ReplyToCommand(client, "[SM] Usage: sm_addsilence <steamid> <time> [reason]");
		return Plugin_Handled;
	}
	total_len += len;
	
	len = BreakString(arguments[total_len], time, sizeof(time));
	
	if (len != -1)
	{
		total_len += len;
	}
	else
	{
		total_len = 0;
		arguments[0] = '\0';
	}
	
	decl String:adminAuth[32];
	
	new minutes = StringToInt(time);
	
	if(!minutes && client && !(HasClientPermissions(client, g_PermanentGagAdminFlags, true)))
	{
		ReplyToCommand(client, "[TG] %t", "No Perm Silence");
		return Plugin_Handled;
	}
	
	if (!client)
	{
		strcopy (adminAuth, sizeof(adminAuth), "STEAM_ID_SERVER");
	}
	else
	{
		GetClientAuthString(client, adminAuth, sizeof(adminAuth));
	}
	
	new Handle:dataPack = CreateDataPack();
	
	WritePackCell(dataPack, client);
	WritePackCell(dataPack, minutes);
	WritePackCell(dataPack, MUTE|GAG);
	WritePackString(dataPack, arguments[total_len]);
	WritePackString(dataPack, authid);
	WritePackString(dataPack, adminAuth);
	
	decl String:query[1024];
	
	FormatEx(query, sizeof(query), "SELECT gagid FROM %s_gag WHERE authid = '%s' AND (length = 0 OR ends > UNIX_TIMESTAMP())", DatabasePrefix, authid);
	SQL_TQuery(hDatabase, SelectAddgagCallback, query, dataPack, DBPrio_High);
	
	FormatEx(query, sizeof(query), "SELECT muteid FROM %s_mute WHERE authid = '%s' AND (length = 0 OR ends > UNIX_TIMESTAMP())", DatabasePrefix, authid);
	SQL_TQuery(hDatabase, SelectAddgagCallback, query, dataPack, DBPrio_High);
	
	return Plugin_Handled;
}

public Action:Command_Reload(client, args)
{
	ResetSettings();
	return Plugin_Handled;
}

public Action:Command_Unmute(client, const String:command[], argc)
{
	if (argc < 1)
		return Plugin_Continue;
		
		
	decl String:arg[64];
	GetCmdArg (1, arg, sizeof(arg));
	
	decl String:targetName[MAX_TARGET_LENGTH];
	new targetList[MAXPLAYERS], targetCount, bool:tn_is_ml;
	
	if ((targetCount = ProcessTargetString(
			arg,
			client, 
			targetList, 
			MAXPLAYERS, 
			0,
			targetName,
			sizeof(targetName),
			tn_is_ml)) <= 0)
	{
		ReplyToTargetError(client, targetCount);
		return Plugin_Handled;
	}
	
	
	for (new i = 0; i < targetCount; i++)
	{
		new target = targetList[i];
		
		if (targetCount == 1 && (g_PlayerStatus[target] & STATE_MUTED))
		{
			ReplyToCommand (client, "[TG] %t", "Target Is Datamuted");
			return Plugin_Handled;
		}
	}
	
	if (targetCount > 1)
	{
		CreateTimer (0.0, RecheckUnmutes, client);
	}
	
	return Plugin_Continue;
}

public Action:Command_Ungag(client, const String:command[], argc)
{
	if (argc < 1)
		return Plugin_Continue;
		
		
	decl String:arg[64];
	GetCmdArg (1, arg, sizeof(arg));
	
	decl String:targetName[MAX_TARGET_LENGTH];
	decl targetList[MAXPLAYERS], targetCount, bool:tn_is_ml;
	
	if ((targetCount = ProcessTargetString(
			arg,
			client, 
			targetList, 
			MAXPLAYERS, 
			0,
			targetName,
			sizeof(targetName),
			tn_is_ml)) <= 0)
	{
		ReplyToTargetError(client, targetCount);
		return Plugin_Handled;
	}
	
	
	for (new i = 0; i < targetCount; i++)
	{
		new target = targetList[i];
		
		if (targetCount == 1 && (g_PlayerStatus[target] & STATE_GAGGED))
		{
			ReplyToCommand (client, "[TG] %t", "Target Is Datagagged");
			return Plugin_Handled;
		}
	}
	
	if (targetCount > 1)
	{
		CreateTimer (0.0, RecheckUngags, client);
	}
	
	return Plugin_Continue;
}

public Action:Command_Unsilence(client, const String:command[], argc)
{
	if (argc < 1)
		return Plugin_Continue;
		
		
	decl String:arg[64];
	GetCmdArg (1, arg, sizeof(arg));
	
	decl String:targetName[MAX_TARGET_LENGTH];
	decl targetList[MAXPLAYERS], targetCount, bool:tn_is_ml;
	
	if ((targetCount = ProcessTargetString(
			arg,
			client, 
			targetList, 
			MAXPLAYERS, 
			0,
			targetName,
			sizeof(targetName),
			tn_is_ml)) <= 0)
	{
		ReplyToTargetError(client, targetCount);
		return Plugin_Handled;
	}
	
	
	for (new i = 0; i < targetCount; i++)
	{
		new target = targetList[i];
		new status = g_PlayerStatus[target];
		
		if (targetCount == 1 && ((status & (STATE_MUTED)) || (status & STATE_GAGGED)))
		{
			if ((status & STATE_MUTED) && (status & STATE_GAGGED))
			{
				ReplyToCommand (client, "[TG] %t", "Target Is Datasilenced");
			}
			else if (status & STATE_MUTED)
			{
				ReplyToCommand (client, "[TG] %t", "Target Is Datamuted");
				BaseComm_SetClientGag(target, false);
			}
			else if (status & STATE_GAGGED)
			{
				ReplyToCommand (client, "[TG] %t", "Target Is Datagagged");
				BaseComm_SetClientMute(target, false);
			}
			return Plugin_Handled;
		}
	}
	
	if (targetCount > 1)
	{
		CreateTimer (0.0, RecheckUnsilences, client);
	}
	
	return Plugin_Continue;
}	

public Action:Command_Say (client, const String:command[], argc)
{
	if (g_OwnReason[client])
	{
		new String:reason[512];
		GetCmdArgString (reason, sizeof(reason));
		StripQuotes(reason);
		
		g_OwnReason[client] = false;
		
		switch (g_GagType[client])
		{
			case GagMenuType_Mute:
			{
				CreateGag (client, g_GagTarget[client], g_GagTime[client], reason, MUTE);
			}
			case GagMenuType_Gag:
			{
				CreateGag (client, g_GagTarget[client], g_GagTime[client], reason, GAG);
			}
			case GagMenuType_Silence:
			{
				CreateGag (client, g_GagTarget[client], g_GagTime[client], reason, GAG|MUTE);
			}
		}
		
		return Plugin_Handled;
	}
	
	return Plugin_Continue;
}

public Action:ProcessQueue(Handle:timer, any:data)
{
	decl String:buffer[512];
	Format(buffer, sizeof(buffer), "SELECT authid, time, starts, reason, type, name, ip, admin FROM queue");
	SQL_TQuery(hSQLiteDB, ProcessQueueCallback, buffer);
}

public Action:ProcessQueueDelete (Handle:timer, any:data)
{
	decl String:query[512];
	Format (query, sizeof(query), "SELECT authid, type FROM queue_delete");
	SQL_TQuery (hSQLiteDB, ProcessQueueDeleteCallback, query);
}

public Action:RecheckUngags (Handle:timer, any:client)
{
	for (new i = 1; i <= MaxClients; i++)
	{
		if (IsClientInGame(i) && !IsFakeClient(i))
		{
			if (!BaseComm_IsClientGagged(i) && (g_PlayerStatus[i] & STATE_GAGGED))
			{
				decl String:name[64];
				GetClientName(i, name, sizeof(name));
				BaseComm_SetClientGag (i, true);
				PrintToChat (client, "[TG] %t", "Regagged Player", name);
			}
		}
	}
	
	return Plugin_Continue;
}

public Action:RecheckUnmutes (Handle:timer, any:client)
{
	for (new i = 1; i <= MaxClients; i++)
	{
		if (IsClientInGame(i) && !IsFakeClient(i))
		{
			if (!BaseComm_IsClientMuted(i) && (g_PlayerStatus[i] & STATE_MUTED))
			{
				decl String:name[64];
				GetClientName(i, name, sizeof(name));
				BaseComm_SetClientMute (i, true);
				PrintToChat (client, "[TG] %t", "Remuted Player", name);
			}
		}
	}
	
	return Plugin_Continue;
}

public Action:RecheckUnsilences (Handle:timer, any:client)
{
	for (new i = 1; i <= MaxClients; i++)
	{
		if (IsClientInGame(i) && !IsFakeClient(i))
		{
			if (!BaseComm_IsClientGagged(i) && (g_PlayerStatus[i] & STATE_GAGGED))
			{
				decl String:name[64];
				GetClientName(i, name, sizeof(name));
				BaseComm_SetClientGag (i, true);
				PrintToChat (client, "[TG] %t", "Regagged Player", name);
			}
			if (!BaseComm_IsClientMuted(i) && (g_PlayerStatus[i] & STATE_MUTED))
			{
				decl String:name[64];
				GetClientName(i, name, sizeof(name));
				BaseComm_SetClientGag (i, true);
				PrintToChat (client, "[TG] %t", "Remuted Player", name);
			}
		}
	}
	
	return Plugin_Continue;
}

public Action:Timer_UngagPlayer (Handle:timer, any:client)
{
	if (!IsClientInGame(client) || IsFakeClient (client))
	{
		g_UngagWhenReadyTimer[client] = INVALID_HANDLE;
		return Plugin_Continue;
	}
	
	BaseComm_SetClientGag (client, false);
	g_PlayerStatus[client] &= ~STATE_GAGGED;
	PrintToChat (client, "[TG] %t", "Now Ungagged");
	
	decl String:authid[32];
	decl String:query[1024];
	
	GetClientAuthString (client, authid, sizeof(authid));
	
	new Handle:dataPack = CreateDataPack();
	
	WritePackCell (dataPack, GAG);
	WritePackString (dataPack, authid);
	ResetPack (dataPack);
	
	Format (query, sizeof(query), "DELETE FROM %s_gag WHERE authid = '%s'", DatabasePrefix, authid);
	SQL_TQuery (hDatabase, VerifyDelete, query, dataPack);
	
	g_UngagWhenReadyTimer[client] = INVALID_HANDLE;
	
	return Plugin_Continue;
}

public Action:Timer_UnmutePlayer (Handle:timer, any:client)
{
	if (!IsClientInGame(client) || IsFakeClient (client))
	{
		g_UnmuteWhenReadyTimer[client] = INVALID_HANDLE;
		return Plugin_Continue;
	}
	
	BaseComm_SetClientMute (client, false);
	g_PlayerStatus[client] &= ~STATE_MUTED;
	PrintToChat (client, "[TG] %t", "Now Unmuted");
	
	decl String:authid[32];
	decl String:query[1024];
	
	GetClientAuthString (client, authid, sizeof(authid));
	
	new Handle:dataPack = CreateDataPack();
	
	WritePackCell (dataPack, MUTE);
	WritePackString (dataPack, authid);
	ResetPack (dataPack);
	
	Format (query, sizeof(query), "DELETE FROM %s_mute WHERE authid = '%s'", DatabasePrefix, authid);
	SQL_TQuery (hDatabase, VerifyDelete, query, dataPack);
	
	g_UnmuteWhenReadyTimer[client] = INVALID_HANDLE;
	
	return Plugin_Continue;
}

public Action:ClientRecheck (Handle:timer, any:client)
{
	if(((g_PlayerStatus[client] & STATE_GAG_UNCHECKED) || (g_PlayerStatus[client] & STATE_MUTE_UNCHECKED)) && IsClientConnected(client))
	{
		decl String:Authid[64];
		GetClientAuthString(client, Authid, sizeof(Authid));
		OnClientAuthorized(client, Authid);
	}

	g_PlayerRecheck[client] =  INVALID_HANDLE;
	return Plugin_Continue;
}

public Action:RetryUngagsTimers (Handle:timer, any:dataPack)
{
	new client = ReadPackCell (dataPack);
	new time = ReadPackCell (dataPack);
	new timeleft;
	GetMapTimeLeft (timeleft);
	
	if (timeleft < 0 && time > 0)
	{
		new Handle:timerPack;
		CreateDataTimer (5.0, RetryUngagsTimers, timerPack, TIMER_FLAG_NO_MAPCHANGE);
		WritePackCell (timerPack, client);
		WritePackCell (timerPack, time-5);
		ResetPack (timerPack);
		return Plugin_Continue;
	}
	
	if (time < 0)
	{
		time = 0;
	}
	
	g_UngagWhenReadyTimer[client] = CreateTimer (float(time), Timer_UngagPlayer, client);
	
	return Plugin_Continue;
}

public Action:RetryUnmutesTimers (Handle:timer, any:dataPack)
{
	new client = ReadPackCell (dataPack);
	new time = ReadPackCell (dataPack);
	new timeleft;
	GetMapTimeLeft (timeleft);
	
	if (timeleft < 0 && time > 0)
	{
		new Handle:timerPack;
		CreateDataTimer (5.0, RetryUnmutesTimers, timerPack, TIMER_FLAG_NO_MAPCHANGE);
		WritePackCell (timerPack, client);
		WritePackCell (timerPack, time-5);
		ResetPack (timerPack);
		return Plugin_Continue;
	}
	
	if (time < 0)
	{
		time = 0;
	}
	
	g_UnmuteWhenReadyTimer[client] = CreateTimer (float(time), Timer_UnmutePlayer, client);
	
	return Plugin_Continue;
}

/********************************************
************** ACTIONS CODE END *************
********************************************/

/********************************************
************** MENU CODE START **************
********************************************/

public OnAdminMenuReady(Handle:topmenu)
{
	/* Block us from being called twice */
	if (topmenu == hTopMenu)
	{
		return;
	}
	
	/* Save the Handle */
	hTopMenu = topmenu;
	
	/* Build the "Player Commands" category */
	new TopMenuObject:playerCommands = FindTopMenuCategory(hTopMenu, ADMINMENU_PLAYERCOMMANDS);
	
	if (playerCommands != INVALID_TOPMENUOBJECT)
	{
		AddToTopMenu(hTopMenu, 
			"sm_timedgag",
			TopMenuObject_Item,
			AdminMenu_Timedgag,
			playerCommands,
			"sm_timedgag",
			0);
	}
}

public AdminMenu_Timedgag (Handle:topmenu, TopMenuAction:action, TopMenuObject:objectId, param, String:buffer[], maxlenght)
{
	if (action == TopMenuAction_DisplayOption)
	{
		Format (buffer, maxlenght, "%T", "Temporarily Gag/Mute", param);
	}
	else if (action == TopMenuAction_SelectOption)
	{
		DisplayGagPlayerMenu (param);
	}
	else if (action == TopMenuAction_DrawOption)
	{
		new flags = GetUserFlagBits (param);
		new bool:canSeeGagMenu = false;
		if ((flags & g_TimedgagAdminFlags) == g_TimedgagAdminFlags
			|| (flags & g_TimedmuteAdminFlags) == g_TimedmuteAdminFlags
			|| (flags & g_TimedsilenceAdminFlags) == g_TimedsilenceAdminFlags
			|| (flags & g_UntimedgagAdminFlags) == g_UntimedgagAdminFlags
			|| (flags & g_UntimedmuteAdminFlags) == g_UntimedmuteAdminFlags
			|| (flags & g_UntimedsilenceAdminFlags) == g_UntimedsilenceAdminFlags
			|| (flags & ADMFLAG_ROOT) == ADMFLAG_ROOT)
		{
			canSeeGagMenu = true;
		}
		
		buffer[0] = canSeeGagMenu ? ITEMDRAW_DEFAULT : ITEMDRAW_IGNORE;
	}
}

DisplayGagPlayerMenu (client)
{
	new Handle:menu = CreateMenu (MenuHandler_GagPlayer, MENU_ACTIONS_DEFAULT|MenuAction_DisplayItem|MenuAction_DrawItem);
	
	decl String:title[128];
	Format (title, sizeof(title), "%T:", "Temporarily Gag/Mute", client);
	SetMenuTitle (menu, title);
	SetMenuExitBackButton (menu, true);
	
	AddTargetsToMenu (menu, client, true, false);
	
	DisplayMenu (menu, client, MENU_TIME_FOREVER);
}

public MenuHandler_GagPlayer (Handle:menu, MenuAction:action, param1, param2)
{
	if (action == MenuAction_End)
	{
		CloseHandle (menu);
	}
	else if (action == MenuAction_Cancel)
	{
		if (param2 == MenuCancel_ExitBack && hTopMenu != INVALID_HANDLE)
		{
			DisplayTopMenu (hTopMenu, param1, TopMenuPosition_LastCategory);
		}
	}
	else if (action == MenuAction_Select)
	{
		decl String:info[32];
		new userid, target;
		
		GetMenuItem (menu, param2, info, sizeof(info));
		userid = StringToInt (info);
		
		target = GetClientOfUserId (userid);
		
		if (target == 0)
		{
			PrintToChat (param1, "[TG] %t", "Player no longer available");
		}
		else if (!CanUserTarget (param1, target))
		{
			PrintToChat (param1, "[TG] %t", "Unable to target");
		}
		else
		{
			g_GagTarget[param1] = target;
			DisplayGagTypesMenu (param1);
		}
	}
	else if (action == MenuAction_DrawItem)
	{
		decl String:info[32];
		GetMenuItem (menu, param2, info, sizeof(info));
		
		new userid = StringToInt (info);
		
		new target = GetClientOfUserId (userid);
		new client = param1;
		
		if (!client)
			return ITEMDRAW_DEFAULT;
		
		if (!CanUserTarget (client, target))
		{
			return ITEMDRAW_IGNORE;
		}
		if (((g_PlayerStatus[target] & STATE_GAGGED) || !HasClientPermissions (client, g_TimedgagAdminFlags, true))
			&& ((g_PlayerStatus[target] & STATE_MUTED) || !HasClientPermissions (client, g_TimedmuteAdminFlags, true))
			&& (!HasClientPermissions (client, g_UntimedgagAdminFlags, true) || g_CommandDisable & DISABLE_UNGAG)
			&& (!HasClientPermissions (client, g_UntimedmuteAdminFlags, true) || g_CommandDisable & DISABLE_UNMUTE)
			&& (!HasClientPermissions (client, g_UntimedgagAdminFlags, true) || g_CommandDisable & DISABLE_UNSILENCE))
		{
			return ITEMDRAW_DISABLED;
		}
		return ITEMDRAW_DEFAULT;
	}
	else if (action == MenuAction_DisplayItem)
	{
		decl String:info[32];
		GetMenuItem (menu, param2, info, sizeof(info));
		
		new userid = StringToInt (info);
		new target = GetClientOfUserId (userid);
		decl String:name[MAX_NAME_LENGTH];
		GetClientName (target, name, sizeof(name));
		
		decl String:display[128];
		
		if ((g_PlayerStatus[target] & (STATE_GAGGED|STATE_MUTED)) == (STATE_GAGGED|STATE_MUTED))
		{
			Format(display, sizeof(display), "%s (silenced)", name);
			return RedrawMenuItem (display);
		}
		else if ((g_PlayerStatus[target] & STATE_GAGGED) == STATE_GAGGED)
		{
			Format(display, sizeof(display), "%s (gagged)", name);
			return RedrawMenuItem (display);
		}
		else if ((g_PlayerStatus[target] & STATE_MUTED) == STATE_MUTED)
		{
			Format(display, sizeof(display), "%s (muted)", name);
			return RedrawMenuItem (display);
		}
		else
		{
			Format(display, sizeof(display), "%s", name);
			return RedrawMenuItem (display);
		}
	}
	return 0;
}

DisplayGagTypesMenu (client)
{
	new Handle:menu = CreateMenu (MenuHandler_GagTypes);
	
	decl String:title[128];
	Format(title, sizeof(title), "%T: %N", "Choose Type", client, g_GagTarget[client]);
	//Format(title, sizeof(title), "Choose Type");
	SetMenuTitle (menu, title);
	SetMenuExitBackButton (menu, true);
	
	new target = g_GagTarget[client];
	
	if (!(g_PlayerStatus[target] & STATE_MUTED) && HasClientPermissions (client, g_TimedmuteAdminFlags, true))
	{
		AddMenuItem (menu, "0", "Mute Player");
	}
	else if (!(g_CommandDisable & DISABLE_UNMUTE) && HasClientPermissions (client, g_UntimedmuteAdminFlags, true))
	{
		AddMenuItem (menu, "1", "Unmute Player");
	}
	if (!(g_PlayerStatus[target] & STATE_GAGGED) && HasClientPermissions (client, g_TimedgagAdminFlags, true))
	{
		AddMenuItem (menu, "2", "Gag Player");
	}
	else if (!(g_CommandDisable & DISABLE_UNGAG) && HasClientPermissions (client, g_UntimedgagAdminFlags, true))
	{
		AddMenuItem (menu, "3", "Ungag Player");
	}
	if ((!(g_PlayerStatus[target] & STATE_GAGGED) || !(g_PlayerStatus[target] & STATE_MUTED)) && HasClientPermissions (client, g_TimedsilenceAdminFlags, true))
	{
		AddMenuItem (menu, "4", "Silence Player");
	}
	else if (!(g_CommandDisable & DISABLE_UNSILENCE) && HasClientPermissions (client, g_UntimedsilenceAdminFlags, true))
	{
		AddMenuItem (menu, "5", "Unsilence Player");
	}
	
	DisplayMenu (menu, client, MENU_TIME_FOREVER);
}

public MenuHandler_GagTypes (Handle:menu, MenuAction:action, param1, param2)
{
	switch (action)
	{
		case MenuAction_End: 
		{
			CloseHandle (menu);
		}
		case MenuAction_Cancel:
		{
			if (param2 == MenuCancel_ExitBack && hTopMenu != INVALID_HANDLE)
			{
				DisplayTopMenu (hTopMenu, param1, TopMenuPosition_LastCategory);
			}
		}
		case MenuAction_Select:
		{
			decl String:info[32];
			new GagMenuType:type;
			
			GetMenuItem (menu, param2, info, sizeof(info));
			type = GagMenuType:StringToInt (info);
			
			g_GagType[param1] = type;
			
			if (type == GagMenuType_Mute || type == GagMenuType_Gag || type == GagMenuType_Silence)
			{
				DisplayMenu (hTimeMenu, param1, MENU_TIME_FOREVER);
			}
			else
			{
				decl String:authid[32];
				new userid;
				GetClientAuthString (g_GagTarget[param1], authid, sizeof(authid));
				userid = GetClientUserId (g_GagTarget[param1]);
				switch (type)
				{
					case GagMenuType_Unmute:
					{
						RemoveGag (param1, authid, MUTE, userid);
					}
					case GagMenuType_Ungag:
					{
						RemoveGag (param1, authid, GAG, userid);
					}
					case GagMenuType_Unsilence:
					{
						RemoveGag (param1, authid, GAG|MUTE, userid);
					}
				}
			}
		}
	}
}

public TimeSelected (Handle:menu, MenuAction:action, param1, param2)
{
	switch (action)
	{
		case MenuAction_Select:
		{
			decl String:info[32];
			new minutes;
			
			GetMenuItem (menu, param2, info, sizeof(info));
			minutes = StringToInt (info);
			
			g_GagTime[param1] = minutes;
			DisplayMenu (hReasonMenu, param1, MENU_TIME_FOREVER);
		}
		case MenuAction_Cancel:
		{
			DisplayGagTypesMenu (param1);
		}
		case MenuAction_DrawItem:
		{
			decl String:info[32];
			new time;
			
			GetMenuItem (menu, param2, info, sizeof(info));
			time = StringToInt (info);
			
			if ((GetClientGagTimeLimit (param1) >=0 && time > GetClientGagTimeLimit (param1)) || (!time && !HasClientPermissions (param1, g_PermanentGagAdminFlags, true)))
			{
				return ITEMDRAW_IGNORE;
			}
			
			return ITEMDRAW_DEFAULT;
		}
	}
	return 0;
}

public ReasonSelected (Handle:menu, MenuAction:action, param1, param2)
{
	switch (action)
	{
		case MenuAction_Cancel:
		{
			if (param2 == MenuCancel_Disconnected)
			{
				if (g_PlayerDataPack[param1] != INVALID_HANDLE)
				{
					CloseHandle (g_PlayerDataPack[param1]);
					g_PlayerDataPack[param1] = INVALID_HANDLE;
				}
			}
			else
			{
				DisplayMenu (hTimeMenu, param1, MENU_TIME_FOREVER);
			}
		}
		case MenuAction_Select:
		{
			decl String:info[128];
			decl String:display[128];
			
			GetMenuItem(menu, param2, info, sizeof(info), _, display, sizeof(display));
			
			if (StrEqual("Own Reason", info))
			{
				g_OwnReason[param1] = true;
				PrintToChat (param1, "[TimedGags] %t", "Chat Reason");
				return;
			}
			
			switch (g_GagType[param1])
			{
				case GagMenuType_Mute:
				{
					CreateGag(param1, g_GagTarget[param1], g_GagTime[param1], display, MUTE);
				}
				case GagMenuType_Gag:
				{
					CreateGag(param1, g_GagTarget[param1], g_GagTime[param1], display, GAG);
				}
				case GagMenuType_Silence:
				{
					CreateGag(param1, g_GagTarget[param1], g_GagTime[param1], display, MUTE|GAG);
				}
			}
		}
	}
}

stock ResetMenu()
{
	if (hReasonMenu != INVALID_HANDLE)
	{
		RemoveAllMenuItems(hReasonMenu);
	}
	if (hTimeMenu != INVALID_HANDLE)
	{
		RemoveAllMenuItems (hTimeMenu);
	}
}

/********************************************
*************** MENU CODE END ***************
********************************************/

/********************************************
************* PARSER CODE START *************
********************************************/

static InitializeConfigParser()
{
	if (ConfigParser == INVALID_HANDLE)
	{
		ConfigParser = SMC_CreateParser();
		SMC_SetReaders(ConfigParser, ReadConfig_NewSection, ReadConfig_KeyValue, ReadConfig_EndSection);
	}
}

static InternalReadConfig(const String:path[])
{
	ConfigState = ConfigStateNone;

	new SMCError:err = SMC_ParseFile(ConfigParser, path);

	if (err != SMCError_Okay)
	{
		decl String:buffer[64];
		if (SMC_GetErrorString(err, buffer, sizeof(buffer)))
		{
			PrintToServer(buffer);
		} else {
			PrintToServer("Fatal parse error");
		}
	}
}

public SMCResult:ReadConfig_NewSection(Handle:smc, const String:name[], bool:opt_quotes)
{
	if(name[0])
	{
		if(strcmp("Config", name, false) == 0)
		{
			ConfigState = ConfigStateConfig;
		} 
		else if(strcmp("GagReasons", name, false) == 0) 
		{
			ConfigState = ConfigStateReasons;
		}
		else if (strcmp ("GagTimeLimits", name, false) == 0)
		{
			ConfigState = ConfigStateLimits;
		}
		else if (strcmp ("GagTimes", name, false) == 0)
		{
			ConfigState = ConfigStateTimes;
		}
	}
	return SMCParse_Continue;
}

public SMCResult:ReadConfig_KeyValue(Handle:smc, const String:key[], const String:value[], bool:key_quotes, bool:value_quotes)
{
	if(!key[0])
		return SMCParse_Continue;

	switch(ConfigState)
	{
		case ConfigStateConfig:
		{
			if (strcmp ("Addgag", key, false) == 0) 
			{
				if (StringToInt(value) == 0)
				{
					g_CommandDisable |= DISABLE_ADDGAG;
				}
			}
			else if (strcmp ("Addmute", key, false) == 0)
			{
				if (StringToInt (value) == 0)
				{
					g_CommandDisable |= DISABLE_ADDMUTE;
				}
			}
			else if (strcmp ("Addsilence", key, false) == 0)
			{
				if (StringToInt (value) == 0)
				{
					g_CommandDisable |= DISABLE_ADDSILENCE;
				}
			}
			else if (strcmp ("Untimedgag", key, false) == 0) 
			{
				if (StringToInt (value) == 0)
				{
					g_CommandDisable |= DISABLE_UNGAG;
				}
			}
			else if (strcmp ("Untimedmute", key, false) == 0) 
			{
				if (StringToInt (value) == 0)
				{
					g_CommandDisable |= DISABLE_UNMUTE;
				}
			}
			else if (strcmp ("Untimedsilence", key, false) == 0) 
			{
				if (StringToInt (value) == 0)
				{
					g_CommandDisable |= DISABLE_UNSILENCE;
				}
			}
			else if (strcmp ("TimedgagAdminFlags", key, false) == 0)
			{
				new len = strlen (value);
				new AdminFlag:flag;
				
				for (new i = 0; i < len; i++)
				{
					if (!FindFlagByChar (value[i], flag))
					{
						LogToFile (log, "Invalid flag detected: %c at %s", value[i], key);
					}
					else
					{
						g_TimedgagAdminFlags |= FlagToBit (flag);
					}
				}
				
				if (g_TimedgagAdminFlags == 0)
				{
					g_TimedgagAdminFlags = ADMFLAG_CHAT;
				}
			}
			else if (strcmp ("TimedmuteAdminFlags", key, false) == 0)
			{
				new len = strlen (value);
				new AdminFlag:flag;
				
				for (new i = 0; i < len; i++)
				{
					if (!FindFlagByChar (value[i], flag))
					{
						LogToFile (log, "Invalid flag detected: %c at %s", value[i], key);
					}
					else
					{
						g_TimedmuteAdminFlags |= FlagToBit (flag);
					}
				}
				
				if (g_TimedmuteAdminFlags == 0)
				{
					g_TimedmuteAdminFlags = ADMFLAG_CHAT;
				}
			}
			else if (strcmp ("TimedsilenceAdminFlags", key, false) == 0)
			{
				new len = strlen (value);
				new AdminFlag:flag;
				
				for (new i = 0; i < len; i++)
				{
					if (!FindFlagByChar (value[i], flag))
					{
						LogToFile (log, "Invalid flag detected: %c at %s", value[i], key);
					}
					else
					{
						g_TimedsilenceAdminFlags |= FlagToBit (flag);
					}
				}
				
				if (g_TimedsilenceAdminFlags == 0)
				{
					g_TimedsilenceAdminFlags = ADMFLAG_CHAT;
				}
			}
			else if (strcmp ("UntimedgagAdminFlags", key, false) == 0)
			{
				new len = strlen (value);
				new AdminFlag:flag;
				
				for (new i = 0; i < len; i++)
				{
					if (!FindFlagByChar (value[i], flag))
					{
						LogToFile (log, "Invalid flag detected: %c at %s", value[i], key);
					}
					else
					{
						g_UntimedgagAdminFlags |= FlagToBit (flag);
					}
				}
				
				if (g_UntimedgagAdminFlags == 0)
				{
					g_UntimedgagAdminFlags = ADMFLAG_UNBAN;
				}
			}
			else if (strcmp ("UntimedmuteAdminFlags", key, false) == 0)
			{
				new len = strlen (value);
				new AdminFlag:flag;
				
				for (new i = 0; i < len; i++)
				{
					if (!FindFlagByChar (value[i], flag))
					{
						LogToFile (log, "Invalid flag detected: %c at %s", value[i], key);
					}
					else
					{
						g_UntimedmuteAdminFlags |= FlagToBit (flag);
					}
				}
				
				if (g_UntimedmuteAdminFlags == 0)
				{
					g_UntimedmuteAdminFlags = ADMFLAG_UNBAN;
				}
			}
			else if (strcmp ("UntimedsilenceAdminFlags", key, false) == 0)
			{
				new len = strlen (value);
				new AdminFlag:flag;
				
				for (new i = 0; i < len; i++)
				{
					if (!FindFlagByChar (value[i], flag))
					{
						LogToFile (log, "Invalid flag detected: %c at %s", value[i], key);
					}
					else
					{
						g_UntimedsilenceAdminFlags |= FlagToBit (flag);
					}
				}
				
				if (g_UntimedsilenceAdminFlags == 0)
				{
					g_UntimedsilenceAdminFlags = ADMFLAG_UNBAN;
				}
			}
			else if (strcmp ("AddgagAdminFlags", key, false) == 0)
			{
				new len = strlen (value);
				new AdminFlag:flag;
				
				for (new i = 0; i < len; i++)
				{
					if (!FindFlagByChar (value[i], flag))
					{
						LogToFile (log, "Invalid flag detected: %c at %s", value[i], key);
					}
					else
					{
						g_AddgagAdminFlags |= FlagToBit (flag);
					}
				}
				
				if (g_AddgagAdminFlags == 0)
				{
					g_AddgagAdminFlags = ADMFLAG_RCON;
				}
			}
			else if (strcmp ("AddmuteAdminFlags", key, false) == 0)
			{
				new len = strlen (value);
				new AdminFlag:flag;
				
				for (new i = 0; i < len; i++)
				{
					if (!FindFlagByChar (value[i], flag))
					{
						LogToFile (log, "Invalid flag detected: %c at %s", value[i], key);
					}
					else
					{
						g_AddmuteAdminFlags |= FlagToBit (flag);
					}
				}
				
				if (g_AddmuteAdminFlags == 0)
				{
					g_AddmuteAdminFlags = ADMFLAG_RCON;
				}
			}
			else if (strcmp ("AddsilenceAdminFlags", key, false) == 0)
			{
				new len = strlen (value);
				new AdminFlag:flag;
				
				for (new i = 0; i < len; i++)
				{
					if (!FindFlagByChar (value[i], flag))
					{
						LogToFile (log, "Invalid flag detected: %c at %s", value[i], key);
					}
					else
					{
						g_AddsilenceAdminFlags |= FlagToBit (flag);
					}
				}
				
				if (g_AddsilenceAdminFlags == 0)
				{
					g_AddsilenceAdminFlags = ADMFLAG_RCON;
				}
			}
			else if (strcmp ("PermanentGagAdminFlags", key, false) == 0)
			{
				new len = strlen (value);
				new AdminFlag:flag;
				
				for (new i = 0; i < len; i++)
				{
					if (!FindFlagByChar (value[i], flag))
					{
						LogToFile (log, "Invalid flag detected: %c at %s", value[i], key);
					}
					else
					{
						g_PermanentGagAdminFlags |= FlagToBit (flag);
					}
				}
				
				if (g_PermanentGagAdminFlags == 0)
				{
					g_PermanentGagAdminFlags = ADMFLAG_ROOT|ADMFLAG_UNBAN;
				}
			}
			/*else if (strcmp ("GagTimeLimit", key, false) == 0)
			{
				g_GagTimeLimit = StringToInt (value);
				
				if (g_GagTimeLimit < 0)
					g_GagTimeLimit = 0;
			}*/
			else if(strcmp("DatabasePrefix", key, false) == 0) 
			{
				strcopy(DatabasePrefix, sizeof(DatabasePrefix), value);

				if(DatabasePrefix[0] == '\0')
				{
					DatabasePrefix = "tg";
				}
			} 
			else if(strcmp("RetryTime", key, false) == 0) 
			{
				g_RetryTime = StringToFloat(value);
				if(g_RetryTime < 15.0)
				{
					g_RetryTime = 15.0;
				} 
				else if(g_RetryTime > 60.0) 
				{
					g_RetryTime = 60.0;
				}
			} 
			else if(strcmp("ProcessQueueTime", key, false) == 0) 
			{
				ProcessQueueTime = StringToInt(value);
			}
			else if(strcmp("ServerID", key, false) == 0)
			{
				ServerID = StringToInt(value);
			}
		}
		case ConfigStateReasons:
		{
			if(hReasonMenu != INVALID_HANDLE)
			{
				AddMenuItem(hReasonMenu, key, value);
			}
		}
		case ConfigStateTimes:
		{
			if (hTimeMenu != INVALID_HANDLE)
			{
				AddMenuItem (hTimeMenu, key, value);
			}
		}
		case ConfigStateLimits:
		{
			SetTrieValue (g_GroupListTrie, key, StringToInt (value));
		}
	}
	return SMCParse_Continue;
}

public SMCResult:ReadConfig_EndSection(Handle:smc)
{
	return SMCParse_Continue;
}

stock ReadConfig()
{
	InitializeConfigParser();

	if (ConfigParser == INVALID_HANDLE)
	{
		return;
	}

	decl String:ConfigFile[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, ConfigFile, sizeof(ConfigFile), "configs/timedgags.cfg");

	if(FileExists(ConfigFile))
	{
		InternalReadConfig(ConfigFile);
		PrintToServer("[SM] Loading configs/timedgags.cfg config file");
	} 
	else 
	{
		decl String:Error[PLATFORM_MAX_PATH + 64];
		FormatEx(Error, sizeof(Error), "[SM] FATAL *** ERROR *** can not find %s", ConfigFile);
		LogToFile(log, "FATAL *** ERROR *** can not find %s", ConfigFile);
		SetFailState(Error);
	}
}

stock ResetSettings()
{
	g_CommandDisable = 0;
	ClearTrie (g_GroupListTrie);

	ResetMenu();
	ReadConfig();
}

/********************************************
************** PARSER CODE END **************
********************************************/


public Native_TGGagPlayer(Handle:plugin, numParams)
{
	new client = GetNativeCell(1);
	new target = GetNativeCell(2);
	new time = GetNativeCell(3);
	decl String:reason[128];
	GetNativeString(4, reason, 128);
	new bool:showReason = GetNativeCell(5);
	
	if(reason[0] == '\0')
		strcopy(reason, sizeof(reason), "Gagged by TimedGags");
	
	CreateGag(client, target, time, reason, GAG, showReason);
	return true;
}

public Native_TGMutePlayer(Handle:plugin, numParams)
{
	new client = GetNativeCell(1);
	new target = GetNativeCell(2);
	new time = GetNativeCell(3);
	decl String:reason[128];
	GetNativeString(4, reason, 128);
	new bool:showReason = GetNativeCell(5);
	
	if(reason[0] == '\0')
		strcopy(reason, sizeof(reason), "Muted by TimedGags");
	
	if(client && IsClientInGame(client))
	{
		new AdminId:aid = GetUserAdmin(client);
		if(aid == INVALID_ADMIN_ID)
		{
			ThrowNativeError(1, "Mute Error: Player is not an admin.");
			return 0;
		}
	}
	
	CreateGag(client, target, time, reason, MUTE, showReason);
	return true;
}

public Native_TGSilencePlayer(Handle:plugin, numParams)
{
	new client = GetNativeCell(1);
	new target = GetNativeCell(2);
	new time = GetNativeCell(3);
	decl String:reason[128];
	GetNativeString(4, reason, 128);
	new bool:showReason = GetNativeCell(5);
	
	if(reason[0] == '\0')
		strcopy(reason, sizeof(reason), "Silenced by TimedGags");
	
	if(client && IsClientInGame(client))
	{
		new AdminId:aid = GetUserAdmin(client);
		if(aid == INVALID_ADMIN_ID)
		{
			ThrowNativeError(1, "Silence Error: Player is not an admin.");
			return 0;
		}
	}
	
	CreateGag(client, target, time, reason, MUTE|GAG, showReason);
	return true;
}